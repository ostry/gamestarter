﻿using System;
using Zenject;
using UnityEngine;
using Claw;
using Claw.UI;
using Claw.Modules;
using Claw.UI.Dialogs;
using GlassCrash.Game.Dialogs;
using GlassCrash.DebugStuff;
using GlassCrash.DebugStuff.DebugCommands;
using Claw.Modules.Controller;
using Claw.AppInput;
using GlassCrash.MainMenu.Dialogs;

namespace GlassCrash.Game
{
    public class GameContext : MonoBehaviour, IModule
    {
        public class Factory : Factory<GameContext>
        {
        }
        
        public Guid InstanceID {get; private set;}

        private DebugCommandsModel debugModel;
        
        private CommandsQueue commandsQueue;
        private TouchAdapter touchManager;
        
        private DialogsManager dialogsManager;
        private ViewsResolver gameViewsResolver;
        private ViewsManager gameViewsManager;
    
        [Inject]
        public GameEvents gameEvents;
        
        [Inject]
        private ControllersFactory controllersFactory;
        
        private GameController gameController;
        
        [Inject]
        public void Inject (
            DebugCommandsModel debugModel,
            CommandsQueue commandsQueue,
            TouchAdapter touchManager)
        {
            this.InstanceID = Guid.NewGuid();
            this.debugModel = debugModel;
            this.commandsQueue = commandsQueue;
            this.touchManager = touchManager;
        }
        
        [Inject]
        public void MapViews(ViewsManager viewsManager)
        {
            viewsManager.HideAllViews();
            
            viewsManager.Map<GameMenuView, GameMenuController>();

            // Dialogs
            viewsManager.Map<DialogsContainerView, DialogsContainerViewController>();

            // LevelWonDialog
            viewsManager.Map<LevelWonDialog, LevelWonDialogController>();

            // LevelLostDialog
            viewsManager.Map<LevelLostDialog, LevelLostDialogController>();
            
            // OutOfLivesDialog
            viewsManager.Map<OutOfLivesDialog, OutOfLivesGameDialogController>();
        }
        
        [Inject]
        public void InitDialogsManager(DialogsManager dialogsManager,
                                       ViewsResolver gameViewsResolver,
                                       ViewsManager gameViewsManager)
        {
            this.dialogsManager = dialogsManager;
            this.gameViewsResolver = gameViewsResolver;
            this.gameViewsManager = gameViewsManager;
        }
        
        private void RegisterDebugCommands()
        {
            debugModel.RegisterCommand(this, new CommandExecution(typeof(WinLevelDebugCommand), commandsQueue));
            debugModel.RegisterCommand(this, new CommandExecution(typeof(LoseLevelDebugCommand), commandsQueue));
        }
        
        private void UnregisterDebugCommands()
        {
            debugModel.UnregisterModule(this);
        }
                
        public void StartGame()
        {
            gameViewsResolver.Resolve<DialogsContainerView>().SetActive(true);
            gameController.InitializeGameplayStateMachine ();
        }
        
        public override string ToString ()
        {
            return this.InstanceID.ToString();
        }
        
        public void Dispose()
        {
        }

        public void Setup()
        {
            RegisterDebugCommands();

            gameController = controllersFactory.CreateController<GameController>();
            gameController.Setup();
        }
        
        public void Teardown()
        {
            gameController.Teardown();
            
            UnregisterDebugCommands();
            dialogsManager.Dispose();
        }
        
        public void UpdateModule(float dt)
        {
            commandsQueue.run();
            touchManager.Update(dt);
            
            if (gameController != null)
            {
                gameController.Update(dt);    
            }
        }
    }
}

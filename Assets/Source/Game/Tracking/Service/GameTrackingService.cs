﻿using System;
using System.Collections.Generic;
using Claw.Tracking;
using GlassCrash.App;
using Zenject;

namespace GlassCrash.Game.Tracking
{
    public class GameTrackingService
    {
        [Inject]
        ITrackingService ts;

        [Inject]
        GameHistoryModel gameHistoryModel;
        
        #region Levels

        public void LogLevelStart()
        {
            int levelId = 1;

            ts.LogLevelStart(levelId);
        }

        public void LogLevelEnd()
        {
            int levelId = 1;
            long score = 123;
            bool success = gameHistoryModel.LastGameResultData != null ?
                                           gameHistoryModel.LastGameResultData.gameResult == GameResult.GAME_WON :
                                           false;

            ts.LogLevelEnd(levelId, score, success);
        }

        private Dictionary<string, object> DefaultParamsPlus(Dictionary<string, object> parameters)
        {
            return parameters;
        }

        public void LevelWon()
        {
            int levelId = 1;
            long score = 123;

            ts.LogEvent("LevelWon", DefaultParamsPlus(new Dictionary<string, object>() {
                { "level", levelId },
                { "score", score }
            }));
        }

        public void LevelLost()
        {
            int levelId = 1;
            long score = 123;

            ts.LogEvent("LevelLost", DefaultParamsPlus(new Dictionary<string, object>() {
                { "level", levelId },
                { "score", score }
            }));
        }

        public void LevelQuit()
        {
            int levelId = 1;
            long score = 123;

            ts.LogEvent("LevelQuit", DefaultParamsPlus(new Dictionary<string, object>() {
                { "level", levelId },
                { "score", score }
            }));
        }

        public void LevelRetryClick()
        {
            int levelId = 1;
            long score = 123;

            ts.LogEvent("LevelRetryClick", DefaultParamsPlus(new Dictionary<string, object>() {
                { "level", levelId },
                { "score", score }
            }));
        }

        #endregion



        #region OutOfMoves

        public void OutOfMovesWatchAdClick()
        {
            int levelId = 1;

            ts.LogEvent("OutOfMovesWatchAd", DefaultParamsPlus(new Dictionary<string, object>() {
                { "levelId", levelId }
            }));
        }

        public void OutOfMovesEndGameClick()
        {
            int levelId = 1;

            ts.LogEvent("OutOfMovesEndGameClick", DefaultParamsPlus(new Dictionary<string, object>() {
                { "levelId", levelId }
            }));
        }

        public void GameOutOfMovesAdRewardAwarded()
        {
            ts.LogEvent("GameOutOfMovesAdRewardAwarded", DefaultParamsPlus(new Dictionary<string, object>() { }));
        }
        
        #endregion


        
        #region OutOfLives

        public void OutOfLives()
        {
            int levelId = 1;
            
            ts.LogEvent("OutOfLives", DefaultParamsPlus(new Dictionary<string, object>() {
                { "levelId", levelId }
            }));
        }
        
        public void OutOfLivesWatchAdClick()
        {
            int levelId = 1;

            ts.LogEvent("OutOfLivesWatchAdClick", DefaultParamsPlus(new Dictionary<string, object>() {
                { "levelId", levelId }
            }));
        }
        
        public void OutOfLivesCancelClick()
        {
            int levelId = 1;
            
            ts.LogEvent("OutOfLivesCancelClick", DefaultParamsPlus(new Dictionary<string, object>() {
                { "levelId", levelId }
            }));
        }

        public void GameOutOfLivesAdRewardAwarded()
        {
            ts.LogEvent("GameOutOfLivesAdRewardAwarded", DefaultParamsPlus(new Dictionary<string, object>() { }));
        }
        
        #endregion
    }
}

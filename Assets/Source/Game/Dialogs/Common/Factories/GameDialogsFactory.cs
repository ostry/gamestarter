﻿using System;
using Claw.UI.Dialogs;
using UnityEngine;
using Zenject;
using Claw.UI;

namespace GlassCrash.Game.Dialogs
{
    public class GameDialogsFactory
    {
        [Inject]
        DialogsFactory dialogsFactory;

        [Inject]
        DiContainer gameContainer;
        
        public ViewType DuplicateDialog<ViewType>(Transform parent = null) where ViewType : MonoView
        {
            var copied = dialogsFactory.DuplicateDialog<ViewType>(parent, gameContainer);
            return copied;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Claw.UI;
using UnityEngine;

namespace GlassCrash.Game.Dialogs
{
    public class DialogStarsView : MonoView
    {
        private List<GameObject> starAnimations = new List<GameObject>();

        public override void Setup()
        {
            InitStarsAnimations();
        }
        
        private void InitStarsAnimations()
        {
            for (int i = 0; i < 3; ++i)
            {
                var starAnim = Resolver.FindGameObjectAnywhere("Stars/StarAnimation" + (i + 1));
                starAnimations.Add(starAnim);

                starAnim.GetComponent<StarLandAnimationView>().SetActive(true);
                starAnim.GetComponent<StarLandAnimationView>().ResetAnimations();
            }
        }
        
        public void PlayStarAnimation(int index, Action onComplete)
        {
            Debug.Log("PlayStarAnimation index:" + index);
            
            var view = starAnimations[index].GetComponent<StarLandAnimationView>();
            view.ResetAnimations();
            view.Animate("StarLand", onComplete);
        }
        
        public void ShowStar(int index)
        {
            starAnimations[index].SetActive(true);
            starAnimations[index].GetComponent<StarLandAnimationView>().ShowStar(true);
        }
    }
}

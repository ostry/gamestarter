﻿using Zenject;
using Claw.UI;
using GlassCrash.App.Audio;
using GlassCrash.Game.Audio;

namespace GlassCrash.Game.Dialogs
{
    public class LevelLostDialogController : ViewController
    {
        [Inject]
        AppAudioRequests appAudioRequests;
        
        private LevelLostDialog view;

        public override void SetView(MonoView monoView)
        {
            view = monoView as LevelLostDialog;
            SetupView();
        }

        public override void Teardown()
        {
            view.viewShownSingal -= PlayShownSound;
        }

        private void SetupView()
        {
            string score = "1234567";
            view.SetLevelScore(score);
            
            view.viewShownSingal += PlayShownSound;
        }
        
        private void PlayShownSound(MonoView monoView)
        {
            appAudioRequests.RequestPlayFX(GameAudioClipsManager.LevelLostDialogSound);
        }
    }
}

﻿using UnityEngine.UI;
using Claw.UI;

namespace GlassCrash.Game.Dialogs
{
    public class LevelLostDialog : DoubleChoiceDialogView
    {
        private Text levelTitle;
        private Text levelScore;
        
        public override void Setup()
        {
            base.Setup();
            
            levelTitle = Resolver.FindGameObjectAnywhere("LevelTitle").GetComponent<Text>();
            levelScore = Resolver.FindGameObjectAnywhere("LevelScore").GetComponent<Text>();
        }
        
        public void SetLevelTitle(string text)
        {
            levelTitle.text = text;
        }
        
        public void SetLevelScore(string text)
        {
            levelScore.text = text;
        }
    }
}

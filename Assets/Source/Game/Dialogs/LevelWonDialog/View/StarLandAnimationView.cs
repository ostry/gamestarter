﻿using System;
using Claw.UI;
using Claw.Animation;

namespace GlassCrash.Game.Dialogs
{
    public class StarLandAnimationView : MonoView
    {
        public override void Setup()
        {
        }
        
        public void ShowStar(bool show)
        {
            Animate("StarInstantOn");
        }
    }
}

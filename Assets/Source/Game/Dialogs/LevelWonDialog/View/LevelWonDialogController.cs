﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Claw;
using Zenject;
using Claw.UI;
using Claw.Animation.Tweening;
using System.Linq.Expressions;
using GlassCrash.App.Audio;
using GlassCrash.Game.Audio;

namespace GlassCrash.Game.Dialogs
{
    public class LevelWonDialogController : ViewController
    {
        [Inject]
        private Juggler appJuggler;
        
        [Inject]
        private AppAudioRequests appAudioRequests;
        
        private Juggler juggler;
        
        private LevelWonDialog view;
        
        public override void SetView(MonoView monoView)
        {
            view = monoView as LevelWonDialog;

            juggler = new Juggler();
            appJuggler.StartTween(juggler);

            SetupView();
        }

        public override void Teardown()
        {
            appJuggler.RemoveTween(juggler);
            
            view.viewShownSingal -= PlayShownSound;
            view.viewShownSingal -= SetupStars;
        }

        private void SetupView()
        {
            SetupTitle();
            SetupScore();
            
            view.viewShownSingal += SetupStars;
            view.viewShownSingal += PlayShownSound;
        }
        
        private void SetupTitle()
        {
            int level = 1;
            view.SetLevelTitle("Level " + level);
        }

        private void SetupScore()
        {
            int score = 1234567;
            view.SetLevelScore(score);
        }
        
        private void SetupStars(MonoView monoView)
        {
            int starsCount = 2;
            for (int i = 0; i < starsCount; i++)
            {
                PlayStarAnimation(i);
            }
        }
        
        private void PlayStarAnimation(int index)
        {
            float delay = 0.3f;
            juggler.StartTween(new CallbackTween(() => { view.PlayStarAnimation(index); }, index * delay));
        }
        
        private void PlayShownSound(MonoView monoView)
        {
            appAudioRequests.RequestPlayFX(GameAudioClipsManager.LevelWonDialogSound);
        }
    }
}

﻿using UnityEngine.UI;
using Claw.UI;
using Claw.UI.Components;
using Claw.Animation;

namespace GlassCrash.Game.Dialogs
{
    public class LevelWonDialog : DoubleChoiceDialogView
    {
        private Text levelTitle;
        private AnimatedText levelScore;
        
        private AnimationController shockAnim;
        
        public DialogStarsView DialogStarsView { get { return ViewsResolver.Resolve<DialogStarsView>(); } }
        
        public override void Setup()
        {
            base.Setup();
            
            levelTitle = Resolver.FindGameObjectAnywhere("LevelTitle").GetComponent<Text>();
            levelScore = Resolver.FindGameObjectAnywhere("LevelScore").GetComponent<AnimatedText>();
            levelScore.value = 0;
            
            shockAnim = Resolver.FindGameObjectAnywhere("ShockPanel").GetComponent<AnimationController>();
            
            DialogStarsView.SetActive(true);
        }
        
        public void SetLevelTitle(string text)
        {
            levelTitle.text = text;
        }
        
        public void SetLevelScore(int score)
        {
            levelScore.CountToValue(score, 1.5f);
        }
        
        public void PlayStarAnimation(int index)
        {
            DialogStarsView.PlayStarAnimation(index, () => {
                shockAnim.Play("Shock");
            });
        }
    }
}

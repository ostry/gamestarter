﻿using Zenject;
using GlassCrash.Game.Tracking;
using Claw.UI;
using GlassCrash.MainMenu.Dialogs;

namespace GlassCrash.Game.Dialogs
{
    public class OutOfLivesGameDialogController : OutOfLivesBaseDialogController
    {
        [Inject]
        GameTrackingService track;

        protected override void OnDialogCancel(DoubleChoiceDialogView dialogView)
        {
            base.OnDialogCancel(dialogView);

            if (ff.IsTrackingEnabled)
            {
                track.OutOfLivesCancelClick();
            }
        }

        protected override void OnWatchAd()
        {
            base.OnWatchAd();

            if (ff.IsTrackingEnabled)
            {
                track.OutOfLivesWatchAdClick();
            }
        }
    }
}

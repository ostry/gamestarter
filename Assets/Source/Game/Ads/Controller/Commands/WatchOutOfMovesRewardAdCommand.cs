﻿using System;
using Claw.Ads;
using Claw;
using Zenject;
using GlassCrash.App.Features;

namespace GlassCrash.Game.Ads
{
    public class WatchOutOfMovesRewardAdCommand : Command
    {
        [Inject]
        FeatureFlags ff;
        
        [Inject]
        AdRewardedService adRewardedService;
        
        public override void Execute()
        {
            if (ff.IsAdsEnabled)
            {
                adRewardedService.PresentRewardAd();    
            }
        }
    }
}

﻿using System;
using Claw.Ads;
using Claw;
using Zenject;

namespace GlassCrash.Game.Ads
{
    public class WatchOutOfMovesInterstitialRewardAdCommand : Command
    {
        [Inject]
        AdInterstitialService adInterstitialService;
        
        public override void Execute()
        {
            adInterstitialService.PresentIntersitial(AdInterstitialService.Interstitial_MainMenu, true);
        }
    }
}

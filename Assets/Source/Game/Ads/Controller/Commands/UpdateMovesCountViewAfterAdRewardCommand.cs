﻿using System;
using Claw;
using Zenject;

namespace GlassCrash.Game.Ads
{
    public class UpdateMovesCountViewAfterAdRewardCommand : Command
    {
        public class Payload
        {
            public int movesCount;

            public Payload(int movesCount)
            {
                this.movesCount = movesCount;
            }
        }
        
        [Inject]
        GameEvents gameEvents;

        [Inject]
        GameRequests gameRequests;

        public override void Execute(object payload)
        {
            var p = payload as Payload;
            
            //gameEvents.NotifyTimeToUpdateMovesCount();
            //gameRequests.RequestPresentMovesCountReward(p.movesCount);
            
            UnityEngine.Debug.Log("AwardUserWithMovesCountCommand executed. payload.movesCount: " + p.movesCount);
        }
    }
}

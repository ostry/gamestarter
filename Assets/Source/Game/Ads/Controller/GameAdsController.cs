﻿using Zenject;
using Claw.Ads;
using Claw;
using Claw.Animation.Tweening;
using GlassCrash.MainMenu.Ads;
using Claw.Modules.Controller;

namespace GlassCrash.Game.Ads
{
    public class GameAdsController : IController
    {
        [Inject]
        AdRewardedService adRewardedService;
        
        [Inject]
        AdInterstitialService adInterstitialService;

        [Inject]
        CommandsQueue commandsQueue;
        
        [Inject]
        Juggler appJuggler;

        [Inject]
        GameRequests gameRequests;
        
        public void Setup()
        {
            adRewardedService.onAdRewarded += OnAdRewarded;
            adInterstitialService.onAdRewarded += OnAdRewarded;
        }
        
        public void Teardown()
        {
            adInterstitialService.onAdRewarded -= OnAdRewarded;
            adRewardedService.onAdRewarded -= OnAdRewarded;
        }
        
        private void OnAdRewarded(IAdRewardedService adService, AdReward reward)
        {
            UnityEngine.Debug.Log("GameAdsController.OnAdRewarded() amount: " + reward.Amount + " type: " + reward.Type);

            //if (gameRequests.IsInState<OutOfLivesState>())
            //{}

            AwardLives(adService, reward);
        }
        
        private void AwardLives(IAdRewardedService adService, AdReward reward)
        {
            int movesCount = (int)reward.Amount;
            commandsQueue.Enqueue<AwardUserWithLivesCountCommand>(new AwardUserWithLivesCountCommand.Payload(movesCount));

            adService.onAdClosedOnce += () =>
            {
                UnityEngine.Debug.Log("GameAdsController.AwardMoves() adRewardedService.onAdClosedOnce()");
                commandsQueue.Enqueue<UpdateLivesCountViewAfterAdRewardCommand>(new UpdateLivesCountViewAfterAdRewardCommand.Payload(movesCount));
            };
        }
    }
}

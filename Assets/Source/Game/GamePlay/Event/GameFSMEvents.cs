﻿using System;
namespace GlassCrash.Game.Events
{
    public class GameFSMEvents
    {
        public event Action gameFSMStateChangedSignal;
        public void NotifyGameFSMStateChanged() { if (gameFSMStateChangedSignal != null) gameFSMStateChangedSignal(); }
    }
}

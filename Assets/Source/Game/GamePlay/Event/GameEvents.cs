﻿using System;

namespace GlassCrash.Game
{
    public class GameEvents
    {
        public event Action gameLevelStartedSignal;
        public event Action gameLevelFinishedSignal;
        public event Action gameExitedSignal;
        
        public event Action timeToUpdateMovesCountSignal;
        public event Action<int> timeToUpdateScoreSignal;
        
        public void NotifyGameLevelStarted()
        {
            if (gameLevelStartedSignal != null)
                gameLevelStartedSignal();
        }
        
        public void NotifyGameLevelFinished()
        {
            if (gameLevelFinishedSignal != null)
                gameLevelFinishedSignal();
        }
        
        public void NotifyGameExited()
        {
            if (gameExitedSignal != null)
                gameExitedSignal();
        }
    }
}

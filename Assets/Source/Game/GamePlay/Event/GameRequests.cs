﻿using System;
namespace GlassCrash.Game
{
    public class GameRequests
    {
        #region FiniteStateMachine

        public event Action<Type> setStateRequest;
        public void RequestSetState<StateT>()
        {
            if (setStateRequest != null)
                setStateRequest(typeof(StateT));
        }

        public event Func<Type[]> getStatesHistoryDelegate;
        public Type[] GetStatesHistory()
        {
            if (getStatesHistoryDelegate != null)
                return getStatesHistoryDelegate();

            throw new Exception("No delegate registered");
        }

        public event Func<Type, bool> isInStateDelegate;
        public bool IsInState<StateT>()
        {
            if (isInStateDelegate != null)
                return isInStateDelegate(typeof(StateT));

            throw new Exception("No delegate registered");
        }

        #endregion
    }
}

﻿using System;
using Claw;
using Claw.FiniteStateMachine;
using GlassCrash.App;
using GlassCrash.Game.Ads;
using Claw.Modules.Controller;
using Zenject;
using GlassCrash.Game.Events;

namespace GlassCrash.Game
{
    public class GameController : IController
    {
        public event Action stateMachineStateChangedSignal;

        private FiniteStateMachine fsm;

        private readonly GameEvents gameEvents;
        private readonly AppRequests appRequests;

        private readonly ControllersFactory controllersFactory;
        private GameAdsController gameAdsController;

        [Inject]
        GameRequests gameRequests;

        [Inject]
        GameFSMEvents gameFSMEvents;

        [Inject]
        StateFactory stateFactory;

        public FiniteStateMachine StateMachine
        {
            get { return fsm; }
        }

        public GameController(ControllersFactory controllersFactory,
                               GameEvents gamePlayEvents,
                               AppRequests appRequests)
        {
            this.gameEvents = gamePlayEvents;
            this.appRequests = appRequests;
            this.controllersFactory = controllersFactory;
        }

        public void Setup()
        {
            this.gameAdsController = controllersFactory.CreateController<GameAdsController>();
            gameAdsController.Setup();

            fsm = new FiniteStateMachine(stateFactory, "Game");
            fsm.stateChangedSignal += OnStateChanged;
            RegisterFiniteStateMachineRequestHandlers();

            RegisterBoardRequestsHandlers();
        }

        public void Teardown()
        {
            UnregisterBoardRequestsHandlers();

            fsm.stateChangedSignal -= OnStateChanged;
            UnregisterFiniteStateMachineRequestHandlers();

            gameAdsController.Teardown();
        }

        public void Update(float dTime)
        {
            if (fsm != null)
            {
                fsm.Update(dTime);
            }
        }

        public void InitializeGameplayStateMachine()
        {
            fsm.SetState<InitGameState>();
        }

        private void OnStateChanged()
        {
            gameFSMEvents.NotifyGameFSMStateChanged();
        }

        private void RegisterBoardRequestsHandlers()
        {
            appRequests.pauseGameInputsRequest += () => {  };
            appRequests.resumeGameInputsRequest += () => {  };
        }

        private void UnregisterBoardRequestsHandlers()
        {
            // TODO: implement !
        }

        private void RegisterFiniteStateMachineRequestHandlers()
        {
            gameRequests.setStateRequest += OnFiniteStateMachine_SetState;
            gameRequests.getStatesHistoryDelegate += OnGetStatesHistoryRequest;
            gameRequests.isInStateDelegate += IsInState;
        }

        private void UnregisterFiniteStateMachineRequestHandlers()
        {
            gameRequests.isInStateDelegate -= IsInState;
            gameRequests.setStateRequest -= OnFiniteStateMachine_SetState;
            gameRequests.getStatesHistoryDelegate -= OnGetStatesHistoryRequest;
        }

        private void OnFiniteStateMachine_SetState(Type stateType)
        {
            // typeof(GoGame<>).MakeGenericType(nextStateType)
            fsm.SetState(stateType);
        }

        private Type[] OnGetStatesHistoryRequest()
        {
            return fsm.GetStatesHistory();
        }
        
        private bool IsInState(Type stateType)
        {
            return fsm.IsInState(stateType);
        }
    }
}

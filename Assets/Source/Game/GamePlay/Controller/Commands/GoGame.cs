﻿using System;
using Claw;
using Zenject;
using Claw.FiniteStateMachine;

namespace GlassCrash.Game
{
    public class GoGame<StateT> : Command where StateT : BaseState
    {
        [Inject]
        GameRequests gameRequests;
        
        public override void Execute()
        {
            gameRequests.RequestSetState<StateT>();
        }
    }
}

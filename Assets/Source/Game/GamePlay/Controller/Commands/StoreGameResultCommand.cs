﻿using System;
using Claw;
using GlassCrash.App;
using Zenject;

namespace GlassCrash.Game
{
    public class StoreGameResultCommand : Command
    {
        public class Payload
        {
            public GameResult gameResult;
            public bool gameUnlockedNewLevel;
            
            public Payload(GameResult gameResult, bool gameUnlockedNewLevel = false)
            {
                this.gameResult = gameResult;
                this.gameUnlockedNewLevel = gameUnlockedNewLevel;
            }
        }
        
        [Inject]
        GameHistoryModel gameHistoryModel;

        public override void Execute(object payload)
        {
            var p = payload as Payload;
            
            GameResultData gameResultData = new GameResultData();
            gameResultData.levelId = 1;
            gameResultData.score = 1234567;
            
            gameResultData.gameResult = p.gameResult;
            gameResultData.gameUnlockedNewLevel = p.gameUnlockedNewLevel;
            
            gameHistoryModel.AddGameResult(gameResultData);
        }
    }
}

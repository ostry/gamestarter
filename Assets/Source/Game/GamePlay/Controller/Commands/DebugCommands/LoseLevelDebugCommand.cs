﻿using System;
using Claw;
using GlassCrash.Game;
using Zenject;

namespace GlassCrash.DebugStuff.DebugCommands
{
    public class LoseLevelDebugCommand : Command
    {
        [Inject]
        CommandsQueue commandsQueue;
        
        public override void Execute()
        {
            commandsQueue.Enqueue<GoGame<LevelLostState>>();
        }
    }
}
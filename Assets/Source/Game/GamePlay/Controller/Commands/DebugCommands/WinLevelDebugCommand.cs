﻿using System;
using Claw;
using Zenject;
using GlassCrash.Game;

namespace GlassCrash.DebugStuff.DebugCommands
{
    public class WinLevelDebugCommand : Command
    {
        [Inject]
        CommandsQueue commandsQueue;
        
        public override void Execute()
        {
            commandsQueue.Enqueue<GoGame<LevelWonState>>();
        }
    }
}

﻿using Zenject;
using GlassCrash.App.User;
using Claw;

namespace GlassCrash.Game
{
    public class StoreLevelStarsCountCommand : Command
    {
        [Inject]
        UserProgressModel userProgressModel;

        public override void Execute()
        {
            int levelId = 1;
            int earnedStars = 2;

            int currentStarsCount = userProgressModel.GetStarsCountForLevelId(levelId);
            if (earnedStars > currentStarsCount)
            {
                userProgressModel.SetStarsCountForLevelId(levelId, earnedStars);
            }
        }
    }
}

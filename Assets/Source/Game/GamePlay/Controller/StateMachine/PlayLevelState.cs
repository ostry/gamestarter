﻿using System;
using UnityEngine;
using Zenject;
using Claw.FiniteStateMachine;
using Claw.UI;
using Claw.Ads;
using GlassCrash.App.Features;
using GlassCrash.Game.Tracking;

namespace GlassCrash.Game
{
    public class PlayLevelState : AnimatedState
    {
        GameMenuView gameMenuView;

        public PlayLevelState(ViewsResolver viewsResolver)
        {
            gameMenuView = viewsResolver.Resolve<GameMenuView>();
        }

        public override void Enter ()
        {
            base.Enter();

            gameMenuView.ExitGameButton.interactable = true;
        }

        public override void Exit()
        {
            base.Exit();

            gameMenuView.ExitGameButton.interactable = false;
        }
    }
}
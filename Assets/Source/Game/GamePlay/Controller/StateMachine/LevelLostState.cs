﻿using Claw;
using Claw.FiniteStateMachine;
using Claw.UI;
using Claw.UI.Dialogs;
using GlassCrash.App.User;
using GlassCrash.Game.Dialogs;
using GlassCrash.App;
using Zenject;
using GlassCrash.App.Features;
using GlassCrash.Game.Tracking;

namespace GlassCrash.Game
{
    public class LevelLostState : AnimatedState
    {
        [Inject]
        FeatureFlags ff;
        
        [Inject]
        GameTrackingService track;
        
        private GameEvents gameEvents;
        private DialogsManager dialogsManager;
        private GameMenuView gameMenuView;

        public LevelLostState(GameEvents gameEvents,
                              ViewsResolver viewsResolver,
                              DialogsManager dialogsManager)
        {
            this.gameEvents = gameEvents;
            this.dialogsManager = dialogsManager;
            this.gameMenuView = viewsResolver.Resolve<GameMenuView>();
        }

        public override void Enter()
        {
            base.Enter();

            UpdateModels();
            
            if (ff.IsAdsEnabled)
            {
                track.LevelLost();
            }
            
            PresentLevelLostDialog();

            gameEvents.NotifyGameLevelFinished();
        }

        private void PresentLevelLostDialog()
        {
            gameMenuView.SetActive(false);
            dialogsManager.PresentDialog<LevelLostDialog>(
                new DialogAction<RetryLevelCommand>(),
                new DialogAction<ExitGameCommand>());
        }

        private void UpdateModels()
        {
            Enqueue<StoreGameResultCommand>(new StoreGameResultCommand.Payload(GameResult.GAME_LOST));
        }
    }
}

﻿using Claw.FiniteStateMachine;

namespace GlassCrash.Game
{
    public class InitGameState : AnimatedState
    {
        public override void Enter ()
        {
            base.Enter();
            Enqueue<GoGame<StartLevelState>>();
        }
    }
}

﻿using Claw.FiniteStateMachine;
using Zenject;
using Claw.UI;
using GlassCrash.Game.Tracking;
using GlassCrash.App.Features;

namespace GlassCrash.Game
{
    public class ExitGameState : AnimatedState
    {
        [Inject]
        FeatureFlags ff;
        
        [Inject]
        GameEvents gameEvents;

        [Inject]
        ViewsResolver viewsResolver;
        
        [Inject]
        GameTrackingService track;

        public override void Enter()
        {
            base.Enter();
            
            if (ff.IsTrackingEnabled)
            {
                track.LogLevelEnd();
            }
            
            viewsResolver.Resolve<GameMenuView>().SetActive(false);
            
            gameEvents.NotifyGameExited();
        }
    }
}

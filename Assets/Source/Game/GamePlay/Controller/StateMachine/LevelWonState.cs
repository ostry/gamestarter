﻿using System;
using UnityEngine;
using Claw;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Claw.FiniteStateMachine;
using Claw.UI;
using GlassCrash.App;
using Claw.UI.Dialogs;
using Claw.Utils;
using GlassCrash.App.User;
using GlassCrash.Game.Dialogs;
using Zenject;
using GlassCrash.App.Features;
using GlassCrash.Game.Tracking;

namespace GlassCrash.Game
{
    public class LevelWonState : AnimatedState
    {
        [Inject]
        FeatureFlags ff;

        [Inject]
        GameTrackingService track;
        
        private GameEvents gameEvents;
        private DialogsManager dialogsManager;
        private GameMenuView gameMenuView;
        
        private UserModel userModel;
        
        public LevelWonState (GameEvents gameEvents,
                              ViewsResolver viewsResolver,
                              DialogsManager dialogsManager,
                              UserModel userModel)
        {
            this.gameEvents = gameEvents;
            this.dialogsManager = dialogsManager;
            this.gameMenuView = viewsResolver.Resolve<GameMenuView>();
            this.userModel = userModel;
        }

        public override void Enter ()
        {
            base.Enter();
            
            UpdateModels();
            
            if (ff.IsAdsEnabled)
            {
                track.LevelWon();
            }

            gameEvents.NotifyGameLevelFinished();

            PresentLevelWonDialog();
        }
        
        private void PresentLevelWonDialog()
        {
            gameMenuView.SetActive(false);
            dialogsManager.PresentDialog<LevelWonDialog>(
                new DialogAction<ExitGameCommand>());
        }
        
        private void UpdateModels()
        {
            bool unlockNewLevel = false;

            Enqueue<StoreLevelStarsCountCommand>();
            Enqueue<StoreGameResultCommand>(new StoreGameResultCommand.Payload(GameResult.GAME_WON, unlockNewLevel));
        }
    }
}

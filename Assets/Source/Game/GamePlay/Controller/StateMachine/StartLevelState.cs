﻿using System;
using UnityEngine;
using Claw.FiniteStateMachine;
using Claw.UI;
using Claw.Ads;
using Zenject;
using GlassCrash.App.Features;
using GlassCrash.Game.Tracking;

namespace GlassCrash.Game
{
    public class StartLevelState : AnimatedState
    {
        [Inject]
        FeatureFlags ff;
        
        [Inject]
        GameEvents gameEvents;

        [Inject]
        GameTrackingService track;

        [Inject]
        AdPresenterProvider adPresenterProvider;
        
        ViewsResolver viewsResolver;
        
        GameMenuView gameMenuView;

        private IAdPresenter InterstialPresenter { get { return adPresenterProvider.GetAdPresenter(AdInterstitialService.Interstitial_StartLevel); } }
        
        public StartLevelState (ViewsResolver viewsResolver)
        {
            this.viewsResolver = viewsResolver;
            
            gameMenuView = viewsResolver.Resolve<GameMenuView>();
        }
        
        public override void Enter ()
        {
            base.Enter();

            if (ff.IsTrackingEnabled)
            {
                track.LogLevelStart();
            }
            
            gameMenuView.SetActive(true);
            ShowBackground();

            gameEvents.NotifyGameLevelStarted();
            
            if (ff.IsStartLevelInterstitialEnabled)
            {
                InterstialPresenter.PresentAd();
            }

            Enqueue<GoGame<PlayLevelState>>();
        }

        private void ShowBackground()
        {
            viewsResolver.ResolveRecursivelyInside<GameBackgroundView>("Backgrounds").ForEach((view) => view.SetActive(false));

            string backgroundName = RandomBackgroundName();
            viewsResolver.Resolve<GameBackgroundView>(backgroundName).SetActive(true);
        }

        private static string[] Backgrounds = {
            "Backgrounds/BackgroundDragon"
        };

        private string RandomBackgroundName()
        {
            int rnd = (int)Math.Floor(UnityEngine.Random.value * (Backgrounds.Length));
            Debug.Log("rnd: " + rnd);
            string backgroundName = Backgrounds[rnd];
            return backgroundName;
        }
    }
}
﻿using System;
using Claw;
using Claw.FiniteStateMachine;
using Claw.UI;
using Claw.UI.Dialogs;
using GlassCrash.App;
using GlassCrash.App.Features;
using GlassCrash.Game.Tracking;
using Zenject;

namespace GlassCrash.Game
{
    public class QuitLevelState : AnimatedState
    {
        [Inject]
        FeatureFlags ff;

        [Inject]
        GameTrackingService track;
        
        private DialogsManager dialogsManager;
        
        public QuitLevelState(DialogsManager dialogsManager)
        {
            this.dialogsManager = dialogsManager;
        }

        public override void Enter()
        {
            base.Enter();
            
            if (ff.IsTrackingEnabled)
            {
                track.LevelQuit();
            }
            
            PresentQuitLevelDialog();
        }

        private void PresentQuitLevelDialog()
        {
            dialogsManager.PresentDialog<QuitLevelDialog>(
                new DialogAction<GoGame<LevelLostState>>(),
                new DialogAction<GoGame<PlayLevelState>>());
        }
    }
}

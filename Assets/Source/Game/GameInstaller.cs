﻿using UnityEngine;
using Zenject;
using Claw.Animation.Tweening;
using Claw;
using Claw.UI;
using ModestTree;
using GlassCrash.Game.GameMenu;
using Claw.UI.Dialogs;
using GlassCrash.Game.Dialogs;
using Claw.Modules.Controller;
using GlassCrash.Game.Events;
using GlassCrash.Game.Tracking;
using Claw.UnityUtils;

namespace GlassCrash.Game
{
    public class GameInstaller : MonoInstaller
    {
        IGameObjectResolver gameResovler;
        
        private void BindUp<T>()
            where T : class
        {
            var instance = Container.TryResolve<T>();
            if (!Container.IsValidating)
            {
                Assert.IsNotNull(instance);
            }
            
            Container.ParentContainers.ForEach(delegate (DiContainer parentContainer){
                parentContainer.Bind<T>().FromInstance(instance).AsSingle();    
            });
        }
        
        public override void InstallBindings()
        {
            InstallCore();
            InstallTracking();
            InstallGame();
        }
        
        private void InstallCore()
        {
            var cameraOrientationObserver = GameObject.Find("Main Camera").GetComponent<CameraOrientationObserver>();
            Container.Bind<CameraOrientationObserver>().FromInstance(cameraOrientationObserver);
            
            gameResovler = new GameObjectResolver(gameObject);
            Container.Bind<IGameObjectResolver>().FromInstance(gameResovler);

            //Controller
            Container.Bind<ControllersFactory>().AsSingle();
            
            // Commands
            Container.Bind<CommandsQueue>().AsSingle().WithArguments("Game");
            Container.Bind<CommandTweenFactory>().AsSingle();
            
            // Views
            Container.Bind<ViewsManager>().AsSingle();
            Container.Bind<ViewsResolver>().AsSingle().WithArguments(gameResovler);
            
            // Dialogs
            Container.Bind<DialogsManager>().AsSingle();

            // Game Finite State Machine
            Container.Bind<StateFactory>().AsSingle();
        }
        
        private void InstallTracking()
        {
            Container.Bind<GameTrackingService>().AsSingle();
        }
        
        private void InstallGame()
        {
            // Game Events
            Container.Bind<GameEvents>().AsSingle();
            Container.Bind<GameFSMEvents>().AsSingle();
            Container.Bind<GameRequests>().AsSingle();
            Container.Bind<GameMenuRequests>().AsSingle();
        }
    }
}

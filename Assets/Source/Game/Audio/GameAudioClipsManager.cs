﻿using System;
using System.Reflection;
using UnityEngine;
using Claw.Utils;

namespace GlassCrash.Game.Audio
{
    public class GameAudioClipsManager : MonoBehaviour
    {
        public const string DynamiteSound = "dynamiteSound";
        public const string RocketSound = "rocketSound";
        public const string PotionSound = "potionSound";
        
        public const string WoodenBoxSound = "woodenBoxSound";
        public const string BubbleSound = "bubbleSound";
        public const string RockSound = "rockSound";
        public const string BarsSound = "barsSound";
        public const string DragonBallSound = "dragonBallSound";
        
        public const string ColorBlockSound = "colorBlockSound";
        
        public const string FlySpawnerSound = "flySpawnerSound";
        
        public const string LevelWonDialogSound = "levelWonDialogSound";
        public const string LevelLostDialogSound = "levelLostDialogSound";
        public const string LevelCelebrationSound = "levelCelebrationSound";

        
        [Header("Board Items Boosters clips")]
        public AudioClip dynamiteSound;
        public AudioClip rocketSound;
        public AudioClip potionSound;

        
        [Header("Board Items Blockers clips")]
        public AudioClip woodenBoxSound;
        public AudioClip bubbleSound;
        public AudioClip rockSound;
        public AudioClip barsSound;
        public AudioClip dragonBallSound;
        
        
        [Header("Board Items Regular clips")]
        public AudioClip colorBlockSound;
        
        
        [Header("Level Celebration")]
        public AudioClip flySpawnerSound;
        
        [Header("Level Dialogs")]
        public AudioClip levelWonDialogSound;
        public AudioClip levelLostDialogSound;
        public AudioClip levelCelebrationSound;



        public AudioClip GetAudioClipByName(string name)
        {
            FileLogger.Log(FileLogger.Audio, "GameAudioClipsManager.GetAudioClipByName() name: " + name);
            FieldInfo filedInfo = this.GetType().GetField(name);
            object fieldInstance = filedInfo.GetValue(this);
            return fieldInstance as AudioClip;
        }
    }
}

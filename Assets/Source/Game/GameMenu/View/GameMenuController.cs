﻿using System;
using UnityEngine;
using Claw;
using Zenject;
using Claw.UI;
using System.Linq;
using GlassCrash.Game.Events;
using GlassCrash.DebugStuff;

namespace GlassCrash.Game
{
    public class GameMenuController : ViewController
    {
        private CommandsQueue commandsQueue;
        private GameEvents gameEvents;

        [Inject]
        DebugModel debugModel;

        [Inject]
        private GameRequests gameRequests;

        [Inject]
        private GameFSMEvents gameFSMEvents;
        
        private GameMenuView view;

        [Inject]
        public void Initialize(CommandsQueue commandsQueue,
                               GameEvents gameEvents)
        {
            this.commandsQueue = commandsQueue;
            this.gameEvents = gameEvents;
        }
        
        public override void SetView(MonoView monoView)
        {
            view = monoView as GameMenuView;
            
            gameFSMEvents.gameFSMStateChangedSignal += OnGameFSMStateChanged;
            
            view.exitGameSignal += OnExitGame;

            ConfigureDebug();
        }

        public override void Teardown()
        {
            view.exitGameSignal -= OnExitGame;

            gameFSMEvents.gameFSMStateChangedSignal -= OnGameFSMStateChanged;
        }

        public void OnExitGame()
        {
            commandsQueue.Enqueue<GoGame<QuitLevelState>>();
        }

        private void OnBoardModelUpdated()
        {
            //IList<IList<IBoardItem>> itemGroups = boardModel.GetItemsGroups(gameLevelModel.MinimumCollectableGroupSize);
            //view.SetGroupCountText("" + itemGroups.Count);
        }
        
        private void OnGameFSMStateChanged()
        {
            Type[] states = gameRequests.GetStatesHistory();
            
            Debug.Log("states: " + states);
            foreach(var stateType in states)
                Debug.Log("stateType: " + stateType);
            
            string value = states.Last().Name;
            view.SetFSMStateText(value);
        }
        
        private void ConfigureDebug()
        {
            view.EnableDebugElements(debugModel.IsDebug);
        }
    }
}
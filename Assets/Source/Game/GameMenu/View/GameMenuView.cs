﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Claw.UI;

namespace GlassCrash.Game
{
    public class GameMenuView : MonoView
    {
        public event Action exitGameSignal;
        public event Action pauseGameSignal;
        
        public Button ExitGameButton { get { return Resolver.FindGameObjectAnywhere("ExitGameButton").GetComponent<Button>(); } }
        public GameObject DebugPanel { get { return Resolver.FindGameObjectAnywhere("DebugPanel"); } }
        
        Text fsmStateText;

        GameObject scoreObject;
        Text scoreText;

        public override void SetActive(bool activate)
        {
            base.SetActive(activate);

            if (activate)
                SetupNestedViews();
        }

        public override void Setup()
        {
            fsmStateText = Resolver.FindGameObjectAnywhere("GameMenuView/DebugPanel/FSMStateLabel").GetComponent<Text>();

            ExitGameButton.gameObject.SetActive(true);
            ExitGameButton.interactable = false;
        }
        
        private void SetupNestedViews()
        {
        }

        /**
         * Perform UI elements deinitialization here.
         */
        public override void Deactivate()
        {
        }
        
        public void SetFSMStateText(string text)
        {
            fsmStateText.text = text;
        }

        public void OnExitGame()
        {
            if (exitGameSignal != null)
                exitGameSignal();
        }
        
        public void OnPauseGame()
        {
            if (pauseGameSignal != null)
                pauseGameSignal();
        }
        
        public void EnableDebugElements(bool enable)
        {
            DebugPanel.SetActive(enable);
        }
    }
}

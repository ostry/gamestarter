﻿using System;
using UnityEngine;
using Claw;
using GlassCrash.Game;

namespace GlassCrash.Game
{
    public class ExitGameCommand : Command
    {
        private CommandsQueue commandsQueue;

        public ExitGameCommand(CommandsQueue commandsQueue)
        {
            this.commandsQueue = commandsQueue;
        }

        public override void Execute()
        {
            commandsQueue.Enqueue<GoGame<ExitGameState>>();
        }
    }
}

﻿using Zenject;
using UnityEngine;
using Claw;
using GlassCrash.App;
using GlassCrash.App.Features;
using GlassCrash.Game.Tracking;

namespace GlassCrash.Game
{
    public class RetryLevelCommand : Command
    {
        [Inject]
        FeatureFlags ff;

        [Inject]
        GameTrackingService track;
        
        [Inject]
        GameHistoryModel gameHistoryModel;

        private LoadingModel loadingModel;
        private CommandsQueue commandsQueue;
        
        public RetryLevelCommand(LoadingModel loadingModel,
                                 CommandsQueue commandsQueue)
        {
            this.loadingModel = loadingModel;
            this.commandsQueue = commandsQueue;
        }

        public override void Execute()
        {
            if (ff.IsTrackingEnabled)
            {
                track.LevelRetryClick();
            }

            AppendLevelRetryRequestToGameHistoryModel();

            commandsQueue.Enqueue<GoGame<ExitGameState>>();
        }

        private void AppendLevelRetryRequestToGameHistoryModel()
        {
            var lastGameResult = gameHistoryModel.LastGameResultData;
            Debug.Assert(lastGameResult != null, "There should be last game result already added.");

            lastGameResult.levelRetryRequested = true;
        }
    }
}

﻿using System;
using Zenject;
using UnityEngine;
using System.Collections;
using Claw;
using GlassCrash;
using GlassCrash.App;
using Claw.UI;
using Claw.UnityUtils;
using GlassCrash.MainMenu;
using GlassCrash.Game;
using Claw.Animation.Tweening;
using Claw.UI.Dialogs;
using Claw.Ads;
using GlassCrash.DebugStuff;
using GlassCrash.App.User;
using Claw.Modules.Services;
using GlassCrash.MainMenu.Ads;
using Claw.Modules.Controller;
using GlassCrash.MainMenu.Events;
using GlassCrash.App.Modules;
using GlassCrash.App.Audio;
using Claw.RemoteConfig;
using GlassCrash.App.Features;
using Claw.Tracking;
using GlassCrash.App.Tracking;
using GlassCrash.MainMenu.Tracking;
using Claw.AppInput;
using GlassCrash.App.RemoteConfig;
using Claw.LocalNotifications;

public class AppInstaller : MonoInstaller
{
    IGameObjectResolver appResolver;

    public override void InstallBindings()
    {
        InstallCore();
        InstallRemoteConfig();
        InstallTracking();
        InstallLocalNotifications();
        InstallDebugStuff();
        InstallAds();
        InstallAppAudio();
        InstallApp();
        InstallViews();

        InstallUser();

        //InstallMainMenu();
    }

    private void InstallCore()
    {
        var appCamera = GameObject.Find("Main Camera");
        var cameraOrientationObserver = appCamera.GetComponent<CameraOrientationObserver>();
        Container.Bind<CameraOrientationObserver>().FromInstance(cameraOrientationObserver);
        Container.Bind<Camera>().FromInstance(appCamera.GetComponent<Camera>());

        SpriteAtlasLoader spriteAtlasLoader = GameObject.Find("AtlasesLoader").GetComponent<SpriteAtlasLoader>();
        Container.Bind<SpriteAtlasLoader>().FromInstance(spriteAtlasLoader).AsSingle();

        Container.Bind<ControllersFactory>().AsSingle();

        Container.Bind<FeatureFlags>().AsSingle();

        Container.Bind<InputAdapter>().AsSingle();

        Container.Bind<VersionService>().AsSingle();
    }

    private void InstallAds()
    {
        Container.Bind<AdInterstitialService>().AsSingle();
        Container.Bind<AdBannerService>().AsSingle();
        Container.Bind<AdRewardedService>().AsSingle();
        Container.Bind<IFakeAdRewardedService>().To<AdRewardedService>().AsSingle();

        Container.Bind<AdPresenterProvider>().AsSingle();
    }

    private void InstallRemoteConfig()
    {
        var defaults = AppRemoteConfigDefaults.GetDefaults();
        Container.Bind<IRemoteConfigService>().To<FirebaseRemoteConfigService>().AsSingle().WithArguments(defaults).NonLazy();
    }

    private void InstallDebugStuff()
    {
        Container.Bind<DebugContext>().AsSingle();
        Container.Bind<DebugController>().AsSingle();

        Container.Bind<DebugModel>().AsSingle();
        Container.Bind<DebugCommandsModel>().AsSingle();

        Container.Bind<DebugViewFactory>().AsSingle();
    }

    private void InstallTracking()
    {
        Container.Bind<ITrackingService>().To<TrackingService>().AsSingle();
        Container.Bind<AppTrackingService>().AsSingle();
    }

    private void InstallLocalNotifications()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        Container.Bind<ILocalNotificationService>().To<MobileLocalNotificationService>().AsSingle();
#elif UNITY_IOS  && !UNITY_EDITOR
        Container.Bind<ILocalNotificationService>().To<MobileLocalNotificationService>().AsSingle();
#else
        Container.Bind<ILocalNotificationService>().To<StubLocalNotificationService>().AsSingle();
#endif
    }

    private void InstallAppAudio()
    {
        // AudioClipsManager is bound by zenject behaviour binding on AppContext/App0/Audio game object.
        // AppAudioSource is bound by zenject behaviour binding on AppContext/App0/Audio game object.
        
        Container.Bind<AppAudioModel>().AsSingle();
        //Container.Bind<AppAudioSource>().AsSingle();
        
        Container.Bind<AppAudioRequests>().AsSingle();
    }
    
    private void InstallApp()
    {
        appResolver = new GameObjectResolver(GameObject.Find("App0").transform);
        Container.Bind<IGameObjectResolver>().FromInstance(appResolver);

        // Context
        Container.BindInterfacesAndSelfTo<AppContext>().AsSingle();

        // Animation
        Container.Bind<Juggler>().AsSingle();

        // Commands
        Container.Bind<CommandsQueue>().AsSingle().WithArguments("App");
        Container.Bind<CommandTweenFactory>().AsSingle();

        // Events
        Container.Bind<AppEvents>().AsSingle();
        Container.Bind<AppRequests>().AsSingle();

        // App Finite State Machine
        Container.Bind<StateFactory> ().AsSingle ();

        // Touch
        Container.Bind<TouchAdapter> ().AsSingle ();
        Container.Bind<GameObjectTouchController> ().AsSingle ();

        // Model
        Container.Bind<LoadingModel>().AsSingle();
        Container.Bind<GameHistoryModel>().AsSingle();
        Container.Bind<AppModulesModel>().AsSingle();
        
        // Service
        Container.Bind<ModelPersistanceService>().AsSingle();
    }

    private void InstallViews()
    {
        Container.Bind<ViewsManager>().AsSingle();

        var appUiResolver = new GameObjectResolver(appResolver.FindGameObject(("App0/UI")));
        Container.Bind<ViewsResolver>().AsSingle().WithArguments(appUiResolver);

        // Dialogs
        Container.Bind<DialogManagerEvents>().AsSingle();
    }

    private void InstallUser()
    {
        // Models
        Container.Bind<UserModel>().AsSingle();
        Container.Bind<UserLivesModel>().AsSingle();
        Container.Bind<UserProgressModel>().AsSingle();
        
        //Events
        Container.Bind<UserEvents>().AsSingle();
        Container.Bind<UserRequests>().AsSingle();
        
        // Services
        Container.Bind<UserLivesService>().AsSingle();
    }
}

﻿namespace GlassCrash.App.LocalNotifications
{
    public class LocalNotificationsIds
    {
        public static readonly int ID_DEBUG = 777777;

        public static readonly int ID_FULL_LIVES_RESTORED = 10;

        public static readonly int ID_TIME_TO_PLAY_NOW = 99;
        public static readonly int ID_TIME_TO_PLAY_ONE_DAY = 100;
        public static readonly int ID_TIME_TO_PLAY_THREE_DAYS = 101;
        public static readonly int ID_TIME_TO_PLAY_EVERY_WEEK = 102;
    }
}

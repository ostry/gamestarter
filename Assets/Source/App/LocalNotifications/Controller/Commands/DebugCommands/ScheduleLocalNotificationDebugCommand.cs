﻿using System;
using Claw;
using Claw.LocalNotifications;
using Zenject;

namespace GlassCrash.App.LocalNotifications
{
    public class ScheduleLocalNotificationDebugCommand : Command
    {
        [Inject]
        ILocalNotificationService localNotificationService;

        public override void Execute()
        {
            Execute("");
        }

        public override void Execute(object payload)
        {
            string p = payload as string;

            float delayInSeconds = ParseDelay(p);

            localNotificationService.Schedule(LocalNotificationsIds.ID_DEBUG, delayInSeconds, "Hey! Debug local notif here :)\n["+DateTime.Now+"]");
        }

        private float ParseDelay(string s)
        {
            double delay = 5.0f;
            if (s == "") return 5.0f;

            return (float)(Double.TryParse(s, out delay) ? delay : delay);
        }
    }
}

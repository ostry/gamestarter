﻿using Zenject;
using Claw;
using Claw.LocalNotifications;
using Claw.RemoteConfig;
using GlassCrash.DebugStuff;

namespace GlassCrash.App.LocalNotifications
{
    public class ScheduleTimeToPlayLocalNotificationsCommand : Command
    {
        [Inject]
        ILocalNotificationService lnService;

        [Inject]
        IRemoteConfigService remoteConfig;

        [Inject]
        DebugModel debugModel;

        private string MessageTimeToPlay { get { return remoteConfig.GetString("local_notif_message_time_to_play"); } }

        public override void Execute()
        {
            if (debugModel.IsDebug)
            {
                lnService.Schedule(LocalNotificationsIds.ID_TIME_TO_PLAY_NOW, 5, MessageTimeToPlay);
            }

            // In one day
            lnService.Schedule(LocalNotificationsIds.ID_TIME_TO_PLAY_ONE_DAY, DaysInSeconds(1), MessageTimeToPlay);

            // In three days
            lnService.Schedule(LocalNotificationsIds.ID_TIME_TO_PLAY_THREE_DAYS, DaysInSeconds(3), MessageTimeToPlay);

            // Every week from now for a year
            for (int i = 1; i <= 52; i++)
            {
                lnService.Schedule(LocalNotificationsIds.ID_TIME_TO_PLAY_EVERY_WEEK + (i-1), WeeksInSeconds(i), MessageTimeToPlay);
            }
        }

        private double DaysInSeconds(double days)
        {
            return 1 * 60 * 60 * 24 * days;
        }

        private double WeeksInSeconds(double weeks)
        {
            return 1 * 60 * 60 * 24 * 7 * weeks;
        }
    }
}

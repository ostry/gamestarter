﻿using System;
using Zenject;
using Claw;
using Claw.LocalNotifications;

namespace GlassCrash.App.LocalNotifications
{
    public class RegisterForLocalNotifications : Command
    {
        [Inject]
        ILocalNotificationService lnService;

        public override void Execute()
        {
            lnService.RegisterForLocalNotifications();
        }
    }
}

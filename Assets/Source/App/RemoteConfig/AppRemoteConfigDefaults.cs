using UnityEngine;
using System.Collections.Generic;

namespace GlassCrash.App.RemoteConfig
{
    public class AppRemoteConfigDefaults
    {
    	public static Dictionary<string, object> GetDefaults()
        {
            Dictionary<string, object> defaults = new Dictionary<string, object>();

            // These are the values that are used if we haven't fetched data from the
            // service yet, or if we ask for values that the service doesn't have:

            defaults.Add("FF_is_tracking_enabled", true);

            defaults.Add("FF_is_ads_enabled", true);
            defaults.Add("FF_is_rewarded_interstitial_enabled", true);

            defaults.Add("FF_is_startlevel_int_enabled", true);
            defaults.Add("FF_is_mainMenu_int_enabled", true);

            defaults.Add("load_ads_interval_in_seconds", 5);

            defaults.Add("mainMenu_ad_int_freq", 2);
            defaults.Add("startLevel_ad_int_freq", 3);

            defaults.Add("life_restore_interval_in_seconds", 5 * 60);
            defaults.Add("max_lives_count", 4);

            defaults.Add("level_intro_view_duration_in_seconds", 1.5);

            defaults.Add("time_scale", 1.0);

            defaults.Add("tell_me_a_joke", "knock knock!");

            defaults.Add("ScrollRectSizeWidthScalePortrait", 1.2f);
            defaults.Add("ScrollRectSizeWidthScaleLandscape", 1.05f);

            defaults.Add("next_episode_release_date", 1538344800000);
            defaults.Add("next_episode_release_message_date", "Next episode coming in:");
            defaults.Add("next_episode_release_message_soon", "Next episode coming soon...");

            defaults.Add("latest_release_version_Other", "1.0.0");
            defaults.Add("latest_release_version_iOS", "1.0.6");
            defaults.Add("latest_release_version_Android", "1.0.7");

            defaults.Add("store_link_to_update_app_Other", "https://www.facebook.com/colorblastpuzzle");
            defaults.Add("store_link_to_update_app_iOS", "itms-apps://itunes.apple.com/app/id1388222364");
            defaults.Add("store_link_to_update_app_Android", "market://details?id=com.igrek.GlassCrash");

            defaults.Add("local_notif_message_time_to_play", "Hey! Time to blast some colorful puzzles! :)");
            defaults.Add("local_notif_message_lives_full", "Hey! All lives are recharged! :)");
            defaults.Add("local_notif_title_android", "Color Blast: Puzzle");


            defaults.Add("FF_fix_IsUnlockNextLevelWhenNewEpisodeIsReleased", true);

            return defaults;
        }
    }
}

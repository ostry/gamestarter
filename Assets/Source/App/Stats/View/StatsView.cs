﻿using System;
using Claw.UI;
using GlassCrash.DebugStuff;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace GlassCrash.App.Stats
{
    public class StatsView : MonoView
    {
        float deltaTime = 0.0f;
        
        public Text FpsText { get { return Resolver.FindGameObjectAnywhere("FpsText").GetComponent<Text>(); } }
        private Text fpsText;

        [Inject]
        DebugModel debugModel;
        
        public override void Setup()
        {
            fpsText = FpsText;
            
            ConfigureDebug();
        }
        
        void Update()
        {
            deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
            UpdateFps();
        }
        
        private void ConfigureDebug()
        {
            if (!debugModel.IsDebug)
            {
                SetActive(false);
            }
        }
        
        private void UpdateFps()
        {
            float msec = deltaTime * 1000.0f;
            float fps = 1.0f / deltaTime;
            string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
            fpsText.text = text;
        }
    }
}

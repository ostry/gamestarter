﻿using System;
using Claw;
using Claw.FiniteStateMachine;
using Zenject;

namespace GlassCrash.App
{
    public class GoAppLoad<StateT> : Command where StateT : BaseState
    {
        [Inject]
        LoadingModel loadingModel;

        [Inject]
        AppRequests appRequests;
        
        public override void Execute()
        {
            appRequests.RequestShowLoadingView(() =>
            {
                loadingModel.SetNextState<StateT>();
                appRequests.RequestSetState<LoadingState>();
            });
        }
    }
}

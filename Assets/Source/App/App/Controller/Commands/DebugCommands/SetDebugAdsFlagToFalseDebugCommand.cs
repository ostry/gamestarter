﻿using System;
using Claw;
using Zenject;
using GlassCrash.DebugStuff;

namespace GlassCrash.App
{
    public class SetDebugAdsFlagToTrueDebugCommand : Command
    {
        [Inject]
		DebugModel debugModel;

        public override void Execute()
        {
            debugModel.IsAdsEnabled = true;
        }
    }
}

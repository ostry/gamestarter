﻿using System;
using Zenject;
using GlassCrash.DebugStuff;
using Claw;

namespace GlassCrash.App
{
    public class SetDebugAdsFlagToFalseDebugCommand : Command
    {
        [Inject]
		DebugModel debugModel;

        public override void Execute()
        {
            debugModel.IsAdsEnabled = false;
        }
    }
}

﻿using System;
using Claw;
using UnityEngine;

namespace GlassCrash.App.Commands.DebugCommands
{
    public class SetGameTimeScaleDebugCommand : Command
    {
        public override void Execute(object payload)
        {
            string p = payload as string;

            float timeScale = ParseTimeScale(p);
            
            var appUpdater = GameObject.Find("AppContext").GetComponent<AppUpdater>();
            appUpdater.timeScale = timeScale;
        }

        private float ParseTimeScale(string s)
        {
            double timeScale = 1.0f;
            if (s == "") return 1.0f;

            return (float) (Double.TryParse(s, out timeScale) ? timeScale : timeScale);
        }
    }
}

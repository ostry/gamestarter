﻿using System;
using Claw;
using UnityEngine;

namespace GlassCrash.App.Commands.DebugCommands
{
    public class SetTargetFrameRateDebugCommand : Command
    {
        public override void Execute(object payload)
        {
            string p = payload as string;

            int fps = ParseFrameRate(p);
            
            var appUpdater = GameObject.Find("AppContext").GetComponent<AppUpdater>();
            appUpdater.targetFrameRate = fps;
        }

        private int ParseFrameRate(string s)
        {
            int fps = 60;
            if (s == "") return 60;

            return Int32.TryParse(s, out fps) ? fps : fps;
        }
    }
}

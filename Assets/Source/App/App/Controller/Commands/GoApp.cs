﻿using System;
using Claw;
using Zenject;
using Claw.FiniteStateMachine;
using System.Diagnostics;

namespace GlassCrash.App
{
    public class GoApp<StateT> : Command where StateT : BaseState
    {
        [Inject]
        AppRequests appRequests;
        
        public override void Execute()
        {
            appRequests.RequestSetState<StateT>();
        }
    }
}

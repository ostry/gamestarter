﻿using System;
using Zenject;
using Claw.Modules.Controller;
using Claw.TimeNS;
using Claw.LocalNotifications;

namespace GlassCrash.App.User
{
    public class AppLocalNotificationsController : IController
    {
        [Inject]
        AppEvents appEvents;

        [Inject]
        ILocalNotificationService localNotificationService;

        public void Setup()
        {
            appEvents.applicationPausedSignal += OnAppPaused;
        }

        public void Teardown()
        {
            appEvents.applicationPausedSignal -= OnAppPaused;
        }

        private void OnAppPaused(bool paused)
        {
            if (!paused)
            {
                localNotificationService.ClearAllReceived();    
            }
        }

    }
}

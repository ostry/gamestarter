﻿using Zenject;
using Claw;
using Claw.FiniteStateMachine;
using Claw.UI;
using Claw.UI.Dialogs;
using GlassCrash.App.Stats;
using GlassCrash.App.Ads;

using GlassCrash.App.LocalNotifications;

namespace GlassCrash.App
{
    public class InitialAppState : AnimatedState
    {
        private CameraOrientationObserver coo;

        //[Inject]
        //DialogsManager dialogsManager;

        private AppRequests appRequests;

        DialogsContainerView dialogsContainerView;
        StatsView statsView;

        public InitialAppState(CameraOrientationObserver coo,
                               AppRequests appRequests,
                               ViewsResolver viewsResolver)
        {
            this.coo = coo;

            this.appRequests = appRequests;

            //this.dialogsContainerView = viewsResolver.Resolve<DialogsContainerView>();
            this.statsView = viewsResolver.Resolve<StatsView>();
        }

        public override void Enter()
        {
            base.Enter();

            appRequests.RequestShowLoadingView(() => {
                InitializeAll();
            });
        }
        
        private void InitializeAll()
        {
            coo.ForceUpdateOrientation();
            
            //ActivateDialogsContainers();
            ActivateStatsView();

            Enqueue<InitializeAdsCommand>();

            Enqueue<RegisterForLocalNotifications>();
            Enqueue<ScheduleTimeToPlayLocalNotificationsCommand>();

            //Enqueue<GoAppLoad<WarmupState>>();
            Enqueue<GoAppLoad<MainMenuState>>();
        }
        
        //private void ActivateDialogsContainers()
        //{
        //    dialogsContainerView.SetActive(true);
        //}
        
        private void ActivateStatsView()
        {
            statsView.SetActive(true);
        }
    }
}

﻿using Claw.Ads;
using Claw.FiniteStateMachine;
using Claw.UI;
using Claw.UI.Audio;
using Claw.UnityUtils;
using GlassCrash.App.Audio;
using GlassCrash.App.Features;
using GlassCrash.App.Modules;
using GlassCrash.MainMenu;
using UnityEngine;
using Zenject;

namespace GlassCrash.App
{
    public class MainMenuState : BaseState
    {
        //[Inject]
        //AppRequests appRequests;

        [Inject]
        AppAudioRequests appAudioRequests;
        
        [Inject]
        AppModulesModel appModulesModel;
        
        [Inject]
        FeatureFlags ff;
        
        MainMenuContext mainMenuContext;
        AdInterstitialService adInterstitialService;
        AdRewardedService adRewardedService;
        Camera camera;

        private MainMenuContainerView mainMenuContainerView;
        
        public MainMenuState(IGameObjectResolver appResolver,
                             MainMenuContext mainMenuContext,
                             Camera camera,
                             AdInterstitialService adInterstitialService,
                             AdRewardedService adRewardedService)
        {
            this.mainMenuContext = mainMenuContext;
            this.camera = camera;
            this.adInterstitialService = adInterstitialService;
            this.adRewardedService = adRewardedService;

            this.mainMenuContainerView = appResolver.FindGameObjectAnywhere("MainMenuContainer").GetComponent<MainMenuContainerView>();
        }

        public override void Enter()
        {
            appModulesModel.MainMenuStateCount++;
            
            appAudioRequests.RequestPlayMusic(AudioClipsManager.SagaMusicSound);

            //if (ff.IsAdsEnabled)
            //{
            //    adRewardedService.RequestRewardedAd();

            //    adInterstitialService.RequestIntersitial(AdInterstitialService.Interstitial_MainMenu);
            //}

            mainMenuContainerView.SetActive(true);
            mainMenuContext.Setup();

            //appRequests.RequestHideLoadingView();
        }

        public override void Exit()
        {
            mainMenuContainerView.SetActive(false);
            mainMenuContext.Teardown();
            
            appAudioRequests.RequestStopMusic(AudioClipsManager.SagaMusicSound);
        }

        public override void Update(float dt)
        {
        }
    }
}

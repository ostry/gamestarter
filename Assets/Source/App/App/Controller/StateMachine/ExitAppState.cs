﻿using Claw.FiniteStateMachine;
using UnityEngine;

namespace GlassCrash.App
{
    public class ExitAppState : AnimatedState
    {
        public override void Enter()
        {
            base.Enter();
            Application.Quit();
        }
    }
}

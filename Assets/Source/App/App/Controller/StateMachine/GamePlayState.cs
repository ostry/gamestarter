﻿using System;
using Claw;
using Claw.FiniteStateMachine;
using Claw.UI;
using GlassCrash.Game;
using Zenject;
using Claw.Ads;
using GlassCrash.App.Audio;
using Claw.UI.Audio;
using GlassCrash.App.Features;
using Claw.UnityUtils;

namespace GlassCrash.App
{
    public class GamePlayState : AnimatedState
    {
        [Inject]
        AppRequests appRequests;
        
        [Inject]
        AppAudioRequests appAudioRequests;
        
        [Inject]
        AdBannerService adBannerService;

        [Inject]
        FeatureFlags ff;
        
        private IGameObjectResolver appResolver;

        [Inject]
        private GameHistoryModel gameHistoryModel;

        private GameContext gameContext;
        private GameEvents gameEvents;

        private GameContainerView gameContainerView;
        
        public GamePlayState(IGameObjectResolver appResolver,
                             GameContext gameContext)
        {
            this.gameContext = gameContext;
            this.gameEvents = gameContext.gameEvents;

            this.gameContainerView = appResolver.FindGameObjectAnywhere("GameContainer").GetComponent<GameContainerView>();
        }

        public override void Enter()
        {
            base.Enter();
            
            appAudioRequests.RequestPlayMusic(AudioClipsManager.GameMusicSound);
            
            gameEvents.gameExitedSignal += OnGameExited;
            
            gameContainerView.SetActive(true);
            gameContext.Setup();
            gameContext.StartGame();
            
            appRequests.RequestHideLoadingView();
            
            if (ff.IsAdsEnabled)
            {
                adBannerService.RequestBannerAd();
                adBannerService.ShowBanner();    
            }
        }

        public override void Exit()
        {
            base.Exit();

            if (ff.IsAdsEnabled)
            {
                adBannerService.HideBanner();
            }
            
            gameEvents.gameExitedSignal -= OnGameExited;
            
            gameContainerView.SetActive(false);
            gameContext.Teardown();
            
            appAudioRequests.RequestStopMusic(AudioClipsManager.GameMusicSound);
        }
        
        private void OnGameExited()
        {
            var lastGameResult = gameHistoryModel.LastGameResultData;
            if (lastGameResult.levelRetryRequested)
            {
                Enqueue<GoAppLoad<GamePlayState>>();
            }
            else
            {
                Enqueue<GoAppLoad<MainMenuState>>();    
            }
        }
    }
}

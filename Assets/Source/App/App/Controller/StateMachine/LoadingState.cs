﻿using System;
using Claw.FiniteStateMachine;
using Claw.UI;

namespace GlassCrash.App
{
    public class LoadingState : AnimatedState
    {
        private LoadingModel loadingModel;
        
        public LoadingState(ViewsResolver viewsResolver,
                            LoadingModel loadingModel)
        {
            this.loadingModel = loadingModel;
        }

        public override void Enter()
        {
            base.Enter();
            EnqueueManagedCallback(ProceedToNextState, 0.0f);
        }

        private void ProceedToNextState()
        {
            Type nextStateType = loadingModel.GetNextState();
            Enqueue(typeof(GoApp<>).MakeGenericType(nextStateType));
        }
    }
}

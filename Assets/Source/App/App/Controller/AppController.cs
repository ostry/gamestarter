﻿using System;
using UnityEngine;
using Claw;
using Claw.UI;
using Claw.FiniteStateMachine;
using Zenject;
using Claw.Ads;
using GoogleMobileAds.Api;
using Claw.UI.Dialogs;
using Claw.Modules.Controller;
using GlassCrash.App.User;
using GlassCrash.App.Audio;
using GlassCrash.App.Ads;
using GlassCrash.App.Features;
using GlassCrash.DebugStuff;
using Claw.Animation.Tweening;

namespace GlassCrash.App
{
    public class AppController : IController
    {
        private const string AdBannerDialogManagerPauseKey = "AppController/DialogsManager";
        private const string AdBannerLoadingViewPauseKey = "AppController/LoadingView";

        [Inject]
        CameraOrientationObserver coo;
        
        [Inject]
        CommandsQueue commandsQueue;

        [Inject]
        Juggler appJuggler;

        [Inject]
        AppRequests appRequests;
        
        [Inject]
        ViewsResolver viewsResolver;
        
        [Inject]
        LoadingModel loadingModel;
        
        [Inject]
        AdBannerService adBannerService;
        
        [Inject]
        StateFactory stateFactory;

        [Inject]
        ControllersFactory controllersFactory;
        
        [Inject]
        DialogManagerEvents dialogManagerEvents;
        
        [Inject]
        FeatureFlags ff;

        UserLivesController userLivesController;
        UserLivesLocalNotificationsController userLivesLocalNotificationsController;

        AppAudioController appAudioController;
        AppLocalNotificationsController appLocalNotificationsController;

        AdsAudioController adsAudioController;
        AdsTrackingController adsTrackingController;
        AdsController adsController;
        
        private FiniteStateMachine fsm;

        public FiniteStateMachine AppStateMachine
        {
            get { return fsm; }
        }

        public void Setup()
        {
            InitializeDebugModel();
            InitializeModels();
            
            InitializeFSM();
            RegisterFiniteStateMachineRequestHandlers();
            
            RegisterAppRequestsHandlers();
            RegisterAdBannerListeners();

            userLivesController = controllersFactory.CreateController<UserLivesController>();
            userLivesController.Setup();

            userLivesLocalNotificationsController = controllersFactory.CreateController<UserLivesLocalNotificationsController>();
            userLivesLocalNotificationsController.Setup();
            
            appAudioController = controllersFactory.CreateController<AppAudioController>();
            appAudioController.Setup();

            appLocalNotificationsController = controllersFactory.CreateController<AppLocalNotificationsController>();
            appLocalNotificationsController.Setup();

            adsAudioController = controllersFactory.CreateController<AdsAudioController>();
            adsAudioController.Setup();
            
            adsTrackingController = controllersFactory.CreateController<AdsTrackingController>();
            adsTrackingController.Setup();
            
            adsController = controllersFactory.CreateController<AdsController>();
            adsController.Setup();
        }

        public void Teardown()
        {
            adsController.Teardown();
            adsTrackingController.Teardown();
            adsAudioController.Teardown();

            appLocalNotificationsController.Teardown();
            appAudioController.Teardown();

            userLivesController.Teardown();
            userLivesLocalNotificationsController.Teardown();
            
            UnregisterFiniteStateMachineRequestHandlers();
            //TODO: implement unregistring ...
        }
        
        public void Update(float dt)
        {
            fsm.Update(dt);
        }
        
        private void InitializeDebugModel()
        {
            commandsQueue.Execute<RegisterDebugModelsPersistanceCommand>();
            commandsQueue.Execute<InitializeDebugModelsCommand>();
        }

        private void InitializeModels()
        {
            commandsQueue.Execute<RegisterAudioModelsPersistanceCommand>();
            commandsQueue.Execute<RegisterUserModelsPersistanceCommand>();

            commandsQueue.Execute<InitializeAudioModelsCommand>();
            commandsQueue.Execute<InitializeUserModelsCommand>();
        }

        private void InitializeFSM()
        {
            fsm = new FiniteStateMachine(stateFactory, "App");
            fsm.SetState<InitialAppState>();
        }
        
        private void RegisterAppRequestsHandlers()
        {
            appRequests.showLoadingViewRequest += (onComplete) =>
            {
                var loadingView = viewsResolver.Resolve<LoadingView>();
                loadingView.Show(onComplete);

                if (ff.IsAdsEnabled)
                {
                    adBannerService.PauseBanner(AdBannerLoadingViewPauseKey);
                }
            };

            appRequests.hideLoadingViewRequest += (onComplete) =>
            {
                appJuggler.StartTween(new CallbackTween(() =>
                {
                    var loadingView = viewsResolver.Resolve<LoadingView>();
                    loadingView.Hide(onComplete);

                    if (ff.IsAdsEnabled)
                    {
                        adBannerService.ResumeBanner(AdBannerLoadingViewPauseKey);
                    }
                }, 0.5f));
            };

            appRequests.playLevelRequest += (onLoadingViewShown) =>
            {
                appRequests.RequestShowLoadingView(() => {
                    if (onLoadingViewShown != null)
                        onLoadingViewShown();

                    loadingModel.SetNextState<GamePlayState>();
                    AppStateMachine.SetState<LoadingState>();
                });
            };

            appRequests.quitApplicationRequest += () =>
            {
                AppStateMachine.SetState<ExitAppState>();
            };
        }
        
        // todo: add handlers unregistering code ...
        private void RegisterAdBannerListeners()
        {
            if (ff.IsAdsEnabled)
            {
                coo.OnOrientationChange += (o) =>
                {
                    var adPosition = o == DeviceOrientation.Portrait ? AdPosition.Bottom : AdPosition.BottomRight;
                    adBannerService.SetPosition(adPosition);
                };

                dialogManagerEvents.dialogWillBePresented += (view) =>
                {
                    adBannerService.PauseBanner(AdBannerDialogManagerPauseKey);
                };

                dialogManagerEvents.dialogWasClosed += (view) =>
                {
                    adBannerService.ResumeBanner(AdBannerDialogManagerPauseKey);
                };
            }
        }
        
        private void RegisterFiniteStateMachineRequestHandlers()
        {
            appRequests.setStateRequest += OnFiniteStateMachine_SetState;
        }
        
        private void UnregisterFiniteStateMachineRequestHandlers()
        {
            appRequests.setStateRequest -= OnFiniteStateMachine_SetState;
        }
        
        private void OnFiniteStateMachine_SetState(Type stateType)
        {
            // typeof(GoApp<>).MakeGenericType(nextStateType)
            fsm.SetState(stateType);
        }
    }
    
}

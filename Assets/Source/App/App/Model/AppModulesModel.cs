﻿using System;

namespace GlassCrash.App.Modules
{
    public class AppModulesModel
    {
        public int MainMenuStateCount { get; set; }
        public bool IsFirstTimeInMainMenu { get { return MainMenuStateCount == 1; } }
    }
}

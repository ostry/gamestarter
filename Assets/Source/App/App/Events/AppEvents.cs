﻿using System;

namespace GlassCrash.App
{
    public class AppEvents
    {
        public event Action<bool> applicationPausedSignal;
        public void NotifyApplicationPaused(bool paused) { if (applicationPausedSignal != null) applicationPausedSignal(paused); }
    }
}

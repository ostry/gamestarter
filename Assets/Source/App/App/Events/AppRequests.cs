﻿using System;

namespace GlassCrash.App
{
    public class AppRequests
    {
        #region Loading View

        public event Action<Action> showLoadingViewRequest;
        public void RequestShowLoadingView(Action onComplete = null)
        {
            if (showLoadingViewRequest != null)
                showLoadingViewRequest(onComplete);
        }

        public event Action<Action> hideLoadingViewRequest;
        public void RequestHideLoadingView(Action onComplete = null)
        {
            if (hideLoadingViewRequest != null)
                hideLoadingViewRequest(onComplete);
        }

        #endregion

        #region Game

        public event Action<Action> playLevelRequest;
        public void RequestPlayLevel(Action onLoadingViewShown = null)
        {
            if (playLevelRequest != null)
                playLevelRequest(onLoadingViewShown);
        }

        #endregion

        #region Input Pausing

        public event Action pauseGameInputsRequest;
        public void RequestPauseGameInputs()
        {
            if (pauseGameInputsRequest != null)
                pauseGameInputsRequest();
        }

        public event Action resumeGameInputsRequest;
        public void RequestResumeGameInputs()
        {
            if (resumeGameInputsRequest != null)
                resumeGameInputsRequest();
        }

        #endregion
        
        #region FiniteStateMachine

        public event Action<Type> setStateRequest;
        public void RequestSetState<StateT>()
        {
            if (setStateRequest != null)
                setStateRequest(typeof(StateT));
        }

        #endregion

        #region Application

        public event Action quitApplicationRequest;
        public void RequestQuitApplication()
        {
            if (quitApplicationRequest != null)
                quitApplicationRequest();
        }

        #endregion
    }
}

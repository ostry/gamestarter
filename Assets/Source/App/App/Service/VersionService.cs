﻿using System;
using Zenject;
using Claw.RemoteConfig;
using UnityEngine;

namespace GlassCrash.App
{
    public class VersionService
    {
        
#if UNITY_IPHONE
        private static string CONFIG_VERSION_KEY = "latest_release_version_iOS";
#elif UNITY_ANDROID
        private static string CONFIG_VERSION_KEY = "latest_release_version_Android";
#else
        private static string CONFIG_VERSION_KEY = "latest_release_version_Other";
#endif

        [Inject]
        IRemoteConfigService remoteConfig;

        public Version RemoteAppVersion {
            get {
                return new Version(remoteConfig.GetString(CONFIG_VERSION_KEY));
            }
        }

        public Version AppVersion {
            get {
                return new Version(Application.version);
            }
        }

        public bool IsNewAppVersionAvailable { get { return RemoteAppVersion.CompareTo(AppVersion) > 0; } }
    }
}

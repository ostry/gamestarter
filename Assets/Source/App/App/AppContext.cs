﻿using Claw;
using Zenject;
using Claw.UI;
using Claw.Modules;
using Claw.Animation.Tweening;
using GlassCrash.DebugStuff;
using GlassCrash.App.User;
using GlassCrash.App.User.Debug;
using Claw.Modules.Controller;
using GlassCrash.App.Audio;
using Claw.Ads;
using Claw.RemoteConfig;
using GlassCrash.App.Commands.DebugCommands;
using Claw.AppInput;
using GlassCrash.App.LocalNotifications;

namespace GlassCrash.App
{
    public class AppContext : IModule
    {
        private DebugCommandsModel debugModel;
        private CommandsQueue commandsQueue;
        private Juggler juggler;
        private ControllersFactory controllersFactory;
        
        [Inject]
        IRemoteConfigService remoteConfig;
        
        [Inject]
        UserLivesService userLivesService;

        [Inject]
        InputAdapter inputAdapter;
        
        private AppController appController;
        
        public AppContext(DebugCommandsModel debugModel,
                          CommandsQueue commandsQueue,
                          Juggler juggler,
                          ControllersFactory controllersFactory)
        {
            this.debugModel = debugModel;
            this.commandsQueue = commandsQueue;
            this.juggler = juggler;
            this.controllersFactory = controllersFactory;
        }
        
        public void Dispose() {}
        
        [Inject]
        private void MapViews(ViewsManager viewsManager)
        {
            viewsManager.HideAllViews();
        }
        
        [Inject]
        private void MapAdsPreneters(AdPresenterProvider adPresenterProvider)
        {
            int mainMenuAdFrequency = remoteConfig.GetInt("mainMenu_ad_int_freq");
            adPresenterProvider.RegisterAdPresenter<AdInterstitialPresenter>(
                AdInterstitialService.Interstitial_MainMenu,
                mainMenuAdFrequency);
            
            int startLevelAdFrequency = remoteConfig.GetInt("startLevel_ad_int_freq");
            adPresenterProvider.RegisterAdPresenter<AdInterstitialPresenter>(
                AdInterstitialService.Interstitial_StartLevel,
                startLevelAdFrequency);
        }
        
        public void Setup()
        {
            appController = controllersFactory.CreateController<AppController>();
            appController.Setup();
            
            RegisterDebugCommands();
        }

        public void Teardown()
        {
            appController.Teardown();
            
            UnregisterDebugCommands();
        }

        public void UpdateModule(float dt)
        {
            UpdateCore(dt);
            UpdateServices(dt);
            UpdateControllers(dt);
        }
        
        private void UpdateCore(float dt)
        {
            commandsQueue.run();
            inputAdapter.Update(dt);
            juggler.Update(dt);
        }
        
        private void UpdateServices(float dt)
        {
            userLivesService.Update(dt);
        }
        
        private void UpdateControllers(float dt)
        {
            if (appController != null)
            {
                appController.Update(dt);
            }
        }
        
        private void RegisterDebugCommands()
        {
            // Debug
            debugModel.RegisterCommand(this, new CommandExecution(typeof(EnableDebugModeCommand), commandsQueue));
            debugModel.RegisterCommand(this, new CommandExecution(typeof(DisableDebugModeCommand), commandsQueue));

            // Debug Ads
            debugModel.RegisterCommand(this, new CommandExecution(typeof(SetDebugAdsFlagToTrueDebugCommand), commandsQueue));
            debugModel.RegisterCommand(this, new CommandExecution(typeof(SetDebugAdsFlagToFalseDebugCommand), commandsQueue));

            // App
            debugModel.RegisterCommand(this, new CommandExecution(typeof(SetGameTimeScaleDebugCommand), commandsQueue));
            debugModel.RegisterCommand(this, new CommandExecution(typeof(SetTargetFrameRateDebugCommand), commandsQueue));
            
            // User
            debugModel.RegisterCommand(this, new CommandExecution(typeof(ResetUserModelsDebugCommand), commandsQueue));

            // Audio
            debugModel.RegisterCommand(this, new CommandExecution(typeof(EnableFxAudioDebugCommand), commandsQueue));
            debugModel.RegisterCommand(this, new CommandExecution(typeof(DisableFxAudioDebugCommand), commandsQueue));

            // LocalNotifications
            debugModel.RegisterCommand(this, new CommandExecution(typeof(ScheduleLocalNotificationDebugCommand), commandsQueue));
        }
        
        private void UnregisterDebugCommands()
        {
            debugModel.UnregisterModule(this);
        }
    }
}

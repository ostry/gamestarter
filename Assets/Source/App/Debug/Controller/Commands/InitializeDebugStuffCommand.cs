﻿using System;
using Claw;
using UnityEngine;

namespace GlassCrash.DebugStuff
{
    public class InitializeDebugStuffCommand : Command
    {
        DebugController debugStuffController;
        
        public InitializeDebugStuffCommand(DebugController debugStuffController)
        {
            this.debugStuffController = debugStuffController;
        }

        public override void Execute()
        {
            debugStuffController.Initialize();
        }
    }
}

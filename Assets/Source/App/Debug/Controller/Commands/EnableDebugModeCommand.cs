﻿using System;
using Claw;
using Zenject;

namespace GlassCrash.DebugStuff
{
    public class EnableDebugModeCommand : Command
    {
        [Inject]
        DebugModel debugModel;
        
        public override void Execute()
        {
            debugModel.IsDebug = true;
        }
    }
}

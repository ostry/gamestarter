﻿using System;
using Claw;
using Claw.Modules.Services;
using GlassCrash.DebugStuff;
using Zenject;

namespace GlassCrash.DebugStuff
{
    public class RegisterDebugModelsPersistanceCommand : Command
    {
        [Inject]
        ModelPersistanceService modelPersistanceService;

        [Inject]
        DebugModel debugModel;
        
        public override void Execute()
        {
            modelPersistanceService.Register(debugModel);
        }
    }
}

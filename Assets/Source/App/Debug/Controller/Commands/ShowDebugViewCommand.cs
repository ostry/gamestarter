﻿using System;
using Claw;
using Claw.UI;

namespace GlassCrash.DebugStuff
{
    public class ShowDebugViewCommand : Command
    {
        ViewsResolver appViewsResolver;
        
        public ShowDebugViewCommand(ViewsResolver appViewsResolver)
        {
            this.appViewsResolver = appViewsResolver;
        }

        public override void Execute()
        {
            var debugView = appViewsResolver.Resolve<DebugView>();
            debugView.SetActive(true);
        }
    }
}

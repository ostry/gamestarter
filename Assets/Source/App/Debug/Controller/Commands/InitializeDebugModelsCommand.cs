﻿using System;
using Claw;
using Claw.Modules.Services;
using Zenject;

namespace GlassCrash.DebugStuff
{
    public class InitializeDebugModelsCommand : Command
    {
        [Inject]
        ModelPersistanceService modelPersistanceService;

        [Inject]
        DebugModel debugModel;

        public override void Execute()
        {
            if (!modelPersistanceService.DeserializeModel(debugModel))
            {
            }
        }
    }
}

﻿using System;
using Claw;
using Claw.UI;
using Zenject;

namespace GlassCrash.DebugStuff
{
    public class HideDebugViewCommand : Command
    {
        [Inject]
        ViewsResolver appViewsResolver;
        
        public override void Execute()
        {
            var debugView = appViewsResolver.Resolve<DebugView>();
            debugView.SetActive(false);
        }
    }
}

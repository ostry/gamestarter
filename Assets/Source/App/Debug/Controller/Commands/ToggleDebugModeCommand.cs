﻿using System;
using Claw;
using Zenject;

namespace GlassCrash.DebugStuff
{
    public class ToggleDebugModeCommand : Command
    {
        [Inject]
        DebugModel debugModel;
        
        public override void Execute()
        {
            debugModel.IsDebug = !debugModel.IsDebug;
        }
    }
}

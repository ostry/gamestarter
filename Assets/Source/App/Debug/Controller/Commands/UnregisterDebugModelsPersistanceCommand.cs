﻿using System;
using Claw;
using Claw.Modules.Services;
using Zenject;

namespace GlassCrash.DebugStuff
{
    public class UnregisterDebugModelsPersistanceCommand : Command
    {
        [Inject]
        ModelPersistanceService modelPersistanceService;

        [Inject]
        DebugModel debugModel;
        
        public override void Execute()
        {
            modelPersistanceService.Unregister(debugModel);
        }
    }
}

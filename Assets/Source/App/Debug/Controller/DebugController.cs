﻿using System;
using Claw;
using UniRx;
using UnityEngine;
using Claw.UI;
using Zenject;
using Claw.AppInput;

namespace GlassCrash.DebugStuff
{
    public class DebugController
    {
        [Inject]
        DebugModel debugModel;
        
        TouchAdapter touchAdapter;
        CommandsQueue commandsQueue;
        
        DebugView debugView;
        
        public DebugController(TouchAdapter touchAdapter,
                               ViewsResolver viewsResolver,
                               CommandsQueue commandsQueue)
        {
            this.touchAdapter = touchAdapter;
            this.commandsQueue = commandsQueue;
            this.debugView = viewsResolver.Resolve<DebugView>();
            
            Initialize();
        }
        
        public void Initialize()
        {
            touchAdapter.OnManyTouch += OnManyTouch;
        }
        
        public void Deinitialize()
        {
            touchAdapter.OnManyTouch -= OnManyTouch;
        }
        
        private void OnManyTouch(int touchCount) {
            
            if (touchCount == 3 && debugModel.IsDebug)
            {
                commandsQueue.Enqueue<ShowDebugViewCommand>();
            }
        }
        
        public void Update(float dt) {}
    }
}

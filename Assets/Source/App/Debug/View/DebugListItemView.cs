﻿using System;
using Claw.UI;
using UnityEngine.UI;

namespace GlassCrash.DebugStuff
{
    public class DebugListItemView : MonoView
    {
        public event Action onButtonSignal;

        public CommandExecution CommandExecution { get; set; }

        public Button Button { get { return Resolver.FindGameObjectAnywhere("Button").GetComponent<Button>(); } }
        public Text ButtonText { get { return Resolver.FindGameObjectAnywhere("Button/Text").GetComponent<Text>(); } }

        public InputField InputField { get { return Resolver.FindGameObjectAnywhere("InputField").GetComponent<InputField>(); } }
        
        public override void Setup()
        {
            Button.onClick.AddListener(() => { if (onButtonSignal != null) onButtonSignal(); });
        }
        
        public override void Deactivate()
        {
            Button.onClick.RemoveAllListeners();
        }
    }
}

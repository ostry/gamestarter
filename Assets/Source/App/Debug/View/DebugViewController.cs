﻿using System;
using Claw;
using Claw.UI;
using Zenject;
using GlassCrash.App;

namespace GlassCrash.DebugStuff
{
    public class DebugViewController : ViewController
    {
        DebugView debugView;
        
        [Inject]
        CommandsQueue commandsQueue;

        [Inject]
        DebugCommandsModel debugModel;
        
        [Inject]
        DebugViewFactory debugViewFactory;

        [Inject]
        AppRequests appRequests;
        
        public override void SetView(MonoView monoView)
        {
            this.debugView = monoView as DebugView;
            this.debugView.onCloseViewSignal += OnCloseView;

            PopulateDebugCommandsList();
         
            appRequests.RequestPauseGameInputs();
        }

        public override void Teardown()
        {
            appRequests.RequestResumeGameInputs();
            
            this.debugView.onCloseViewSignal -= OnCloseView;
        }
        
        private void PopulateDebugCommandsList()
        {
            foreach (var module in debugModel.registeredModules.Keys)
            {
                foreach (var commandExecution in debugModel.registeredModules[module])
                {
                    var debugItemView = debugViewFactory.CreateDebugListItemView(commandExecution);
                    debugView.AddDebugListItemView(debugItemView);
                }    
            }
        }
        
        private void OnCloseView() {
            commandsQueue.Enqueue<HideDebugViewCommand>();
        }
    }
}

﻿using System;
using UnityEngine;
using Zenject;

namespace GlassCrash.DebugStuff
{
    public class DebugViewFactory
    {
        [Inject]
        DiContainer container;
        
        public DebugListItemView CreateDebugListItemView(CommandExecution commandExecution)
        {
            string path = "App/Debug/Prefabs/DebugListItemView";
            GameObject instance = GameObject.Instantiate(Resources.Load(path, typeof(GameObject))) as GameObject;
            
            container.InjectGameObject(instance);
            
            var view = instance.GetComponent<DebugListItemView>();
            view.CommandExecution = commandExecution;
            view.SetActive(false);
            
            return view;
        }
    }
}

﻿using System;
using Claw.UI;
using Claw;
using UnityEngine;
using Zenject;
using Claw.UnityUtils;

namespace GlassCrash.DebugStuff
{
    public class DebugView : MonoView
    {
        public Action onCloseViewSignal;

        GameObject listContent;
        
        public override void Setup()
        {
            listContent = Resolver.FindGameObjectAnywhere("ListContent");
            DestroyCommandsItems();
        }
        
        public void CloseView()
        {
            if (onCloseViewSignal != null)
                onCloseViewSignal();
        }
        
        private void DestroyCommandsItems()
        {
            GameObjectVisitor.ListChildren(listContent.transform).ForEach((child =>
            {
                child.transform.SetParent(null);
                GameObject.Destroy(child.gameObject);
            }));
        }
        
        public void AddDebugListItemView(DebugListItemView debugItemView)
        {
            debugItemView.Root.transform.SetParent(listContent.transform, false);
            debugItemView.Root.transform.SetAsFirstSibling();

            debugItemView.SetActive(true);
        }

        
    }
}

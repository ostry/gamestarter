﻿using System;
using Claw.UI;
using Claw;
using Zenject;

namespace GlassCrash.DebugStuff
{
    public class DebugListItemViewController : ViewController
    {
        DebugListItemView view;

        [Inject]
        CommandsQueue commandsQueue;

        public override void SetView(MonoView monoView)
        {
            view = monoView as DebugListItemView;
            view.onButtonSignal += OnButton;

            view.ButtonText.text = view.CommandExecution.commandType.Name;
        }

        public override void Teardown()
        {
            view.onButtonSignal -= OnButton;
        }

        private void OnButton()
        {
            CommandsQueue execCommandsQueue = view.CommandExecution.commandsQueue;
            Type commandType = view.CommandExecution.commandType;
            string payloadText = view.InputField.text;
            
            if (payloadText != "")
            {
                execCommandsQueue.Enqueue(commandType, payloadText);
            }
            else
            {
                execCommandsQueue.Enqueue(commandType);
            }
            
            commandsQueue.Enqueue<HideDebugViewCommand>();
        }
    }
}

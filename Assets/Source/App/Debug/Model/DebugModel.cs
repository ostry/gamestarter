﻿using System;
using Claw.Modules.Models;

namespace GlassCrash.DebugStuff
{
    public class DebugModel : IPersistedModel
    {
        #region IPersistedModel

        public event Action<IPersistedModel> serializeModelSignal;
        private void RequestSerialization()
        {
            if (serializeModelSignal != null)
                serializeModelSignal(this);
        }

        #endregion
        
        public event Action isDebugChangedSignal;
        private void NotifyIsDebugChanged() { if (isDebugChangedSignal != null) isDebugChangedSignal();
         }

        private bool isDebug = false;
        public bool IsDebug { 
            get {
                return isDebug; 
            }
            
            set {
                isDebug = value;
                RequestSerialization();
                NotifyIsDebugChanged();
            } 
        }

        private bool isAdsEnabled = true;
        public bool IsAdsEnabled
        {
            get
            {
                return isAdsEnabled;
            }

            set
            {
                isAdsEnabled = value;
                RequestSerialization();
                NotifyIsDebugChanged();
            }
        }
        
        public void ResetModel()
        {
            isDebug = false;
            isAdsEnabled = true;
            RequestSerialization();
        }
    }
}

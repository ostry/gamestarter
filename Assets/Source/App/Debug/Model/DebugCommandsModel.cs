﻿using System;
using System.Collections.Generic;
using GlassCrash.DebugStuff.DebugCommands;
using Claw;
using Claw.Modules;

namespace GlassCrash.DebugStuff
{
    public class CommandExecution
    {
        public Type commandType;
        public object commandPayload;
        public CommandsQueue commandsQueue;
        
        public CommandExecution(Type commandType, CommandsQueue commandsQueue)
        {
            this.commandType = commandType;
            this.commandsQueue = commandsQueue;
        }
    }

    public class DebugCommandsModel
    {
        public Dictionary<IModule, List<CommandExecution>> registeredModules = new Dictionary<IModule, List<CommandExecution>>();

        #region DebugCommands

        private List<CommandExecution> GetCommandList(IModule module)
        {
            if (!registeredModules.ContainsKey(module))
            {
                registeredModules[module] = new List<CommandExecution>();
            }
            return registeredModules[module];
        }

        public void RegisterCommand(IModule module, CommandExecution commandExec)
        {
            GetCommandList(module).Add(commandExec);
        }

        public void UnregisterModule(IModule module)
        {
            GetCommandList(module).Clear();
        }

        #endregion
    }
}

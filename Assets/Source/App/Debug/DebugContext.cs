﻿using System;
using Claw.Modules;
using Claw.UI;
using UnityEngine;
using Zenject;

namespace GlassCrash.DebugStuff
{
    public class DebugContext : IModule
    {
        DebugController debugController;
        
        public DebugContext(DebugController debugController)
        {
            this.debugController = debugController;
            
            debugController.Initialize();
        }

        public void Dispose()
        {
            debugController.Deinitialize();
        }
        
        [Inject]
        private void MapViews(ViewsManager viewsManager)
        {
            viewsManager.HideAllViews();

            viewsManager.Map<DebugView, DebugViewController>();
            viewsManager.Map<DebugListItemView, DebugListItemViewController>();
        }

        public void Setup() {}
        public void Teardown() {}
        
        public void UpdateModule(float dt)
        {
            debugController.Update(dt);
        }
    }
}

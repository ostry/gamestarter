﻿using System;

namespace GlassCrash.App
{
    public class LoadingModel
    {
        private Type nextState;
        
        public void SetNextState<T>()
        {
            nextState = typeof(T);
        }
        
        public Type GetNextState()
        {
            return nextState;
        }
    }
}

﻿using System;
using Claw;
using Claw.Modules.Services;
using Zenject;

namespace GlassCrash.App.User
{
    public class UnregisterUserModelsPersistanceCommand : Command
    {
        [Inject]
        ModelPersistanceService modelPersistanceService;

        [Inject]
        UserModel userModel;
        
        [Inject]
        UserLivesModel userLivesModel;

        [Inject]
        UserProgressModel userProgressModel;
        
        public override void Execute()
        {
            modelPersistanceService.Unregister(userModel);
            modelPersistanceService.Unregister(userLivesModel);
            modelPersistanceService.Unregister(userProgressModel);
        }
    }
}

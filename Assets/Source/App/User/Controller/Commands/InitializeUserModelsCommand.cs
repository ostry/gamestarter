﻿using System;
using Claw;
using Claw.Modules.Services;
using Zenject;

namespace GlassCrash.App.User
{
    public class InitializeUserModelsCommand : Command
    {
        [Inject]
        ModelPersistanceService modelPersistanceService;

        [Inject]
        UserModel userModel;

        [Inject]
        UserLivesModel userLivesModel;

        [Inject]
        UserProgressModel userProgressModel;

        public override void Execute()
        {
            if (!modelPersistanceService.DeserializeModel(userModel))
            {
                userModel.ResetModel();
            }
            
            if (!modelPersistanceService.DeserializeModel(userLivesModel))
            {
            }
            
            if (!modelPersistanceService.DeserializeModel(userProgressModel))
            {
            }
        }
    }
}

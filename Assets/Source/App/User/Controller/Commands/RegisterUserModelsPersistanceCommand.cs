﻿using System;
using Claw;
using Claw.Modules.Services;
using Zenject;

namespace GlassCrash.App.User
{
    public class RegisterUserModelsPersistanceCommand : Command
    {
        [Inject]
        ModelPersistanceService modelPersistanceService;

        [Inject]
        UserModel userModel;
        
        [Inject]
        UserLivesModel userLivesModel;
        
        [Inject]
        UserProgressModel userProgressModel;
        
        public override void Execute()
        {
            modelPersistanceService.Register(userModel);
            modelPersistanceService.Register(userLivesModel);
            modelPersistanceService.Register(userProgressModel);
        }
    }
}

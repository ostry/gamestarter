﻿using System;
using Claw;
using Zenject;

namespace GlassCrash.App.User.Debug
{
    public class ResetUserModelsDebugCommand : Command
    {
        [Inject]
        UserModel userModel;
        
        [Inject]
        UserLivesModel userLivesModel;

        [Inject]
        UserProgressModel userProgressModel;
        
        public override void Execute()
        {
            userModel.ResetModel();
            userLivesModel.ResetModel();
            userProgressModel.ResetModel();
        }
    }
}

﻿using System;
using Claw;
using Zenject;

namespace GlassCrash.App.User
{
    public class LooseLifeCommand : Command
    {
        [Inject]
        UserLivesModel userLivesModel;
        
        public override void Execute()
        {
            userLivesModel.LooseLife();
        }
    }
}

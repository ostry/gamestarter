﻿using System;
using Claw.Modules.Controller;
using Zenject;
using Claw.RemoteConfig;
using Claw.Utils;
using System.Linq;

namespace GlassCrash.App.User
{
    public class UserLivesController : IController
    {
        [Inject]
        IRemoteConfigService remoteConfig;
        
        [Inject]
        UserLivesModel userLivesModel;
        
        [Inject]
        UserRequests userRequests;

        [Inject]
        UserLivesService userLivesService;
        
        public void Setup()
        {
            userRequests.lifeRestorationRequest += OnLifeRestorationRequest;
            userLivesModel.livesCountChangedSignal += OnLivesCountChanged;
            userRequests.nextLifeRestorationRemainingTimeInSecondsDelegate += OnNextLifeRestorationRemainingTimeInSecondsDelegate;
            
            userLivesService.Start();
            OnLivesCountChanged();
        }
        
        public void Teardown()
        {
            userLivesService.Stop();
            
            userRequests.nextLifeRestorationRemainingTimeInSecondsDelegate -= OnNextLifeRestorationRemainingTimeInSecondsDelegate;
            userLivesModel.livesCountChangedSignal -= OnLivesCountChanged;
            userRequests.lifeRestorationRequest -= OnLifeRestorationRequest;
        }
        
        private void OnLifeRestorationRequest()
        {
            userLivesModel.RestoreSingleLife();
        }
        
        private void OnLivesCountChanged()
        {
            if (userLivesModel.IsLivesFull)
            {
                userLivesService.CancellAllScheduledLivesRestorations();
            }
            else
            {
                double lifeRestoreIntervalInSeconds = remoteConfig.GetDouble("life_restore_interval_in_seconds");
                double maxPendingLifeRestorationTimestampInSeconds = userLivesService.PendingLivesRestorationsCount>0 ? userLivesService.RestorationTimesInSeconds.Last() : 0;

                int missingLives = userLivesModel.MaxLivesCount - userLivesModel.LivesCount;
                int missingLivesRestorations = Math.Max(0, missingLives - userLivesService.PendingLivesRestorationsCount);
                for (int i = 1; i <= missingLivesRestorations; i++)
                {
                    double intervalInSeconds = maxPendingLifeRestorationTimestampInSeconds + lifeRestoreIntervalInSeconds * i;
                    userLivesService.RestoreLifeInSeconds(intervalInSeconds * i);

                    FileLogger.Log("Restoring life in " + intervalInSeconds + " sec.");
                }
            }
        }
        
        private double OnNextLifeRestorationRemainingTimeInSecondsDelegate()
        {
            return userLivesService.TimeRemainingForNextLifeRestorationInSeconds();
        }
    }
}

﻿using System;
using Claw.Modules.Controller;
using Zenject;
using GlassCrash.App.User;
using System.Collections.Generic;
using Claw.TimeNS;
using Claw.LocalNotifications;
using GlassCrash.App.LocalNotifications;
using Claw.RemoteConfig;

namespace GlassCrash.App.User
{
    public class UserLivesLocalNotificationsController : IController
    {
        [Inject]
        UserLivesModel userLivesModel;

        [Inject]
        UserEvents userEvents;

        [Inject]
        UserLivesService userLivesService;

        [Inject]
        ILocalNotificationService localNotificationService;

        [Inject]
        IRemoteConfigService remoteConfig;

        private string LivesFullMessage { get { return remoteConfig.GetString("local_notif_message_lives_full"); } }

        public void Setup()
        {
            userEvents.userLifeRestoredSignal += ScheduleLivesAreFullLocalNotification;
            userEvents.userLifeRestorationScheduledSignal += ScheduleLivesAreFullLocalNotification;
            userEvents.userLifeRestorationsAllCanceledSignal += OnUserLifeRestorationsAllCanceled;
        }

        public void Teardown()
        {
            userEvents.userLifeRestoredSignal -= ScheduleLivesAreFullLocalNotification;
            userEvents.userLifeRestorationScheduledSignal -= ScheduleLivesAreFullLocalNotification;
            userEvents.userLifeRestorationsAllCanceledSignal -= OnUserLifeRestorationsAllCanceled;
        }

        private void ScheduleLivesAreFullLocalNotification()
        {
            int missingLives = userLivesModel.MaxLivesCount - userLivesModel.LivesCount;
            if (missingLives < 1)
                return;

            if (userLivesService.PendingLivesRestorationsCount >= missingLives)
            {
                List<double> restorationTimes = userLivesService.RestorationTimesInSeconds;
                double fullLivesRestorationTimeInSeconds = restorationTimes[missingLives - 1];

                localNotificationService.Schedule(LocalNotificationsIds.ID_FULL_LIVES_RESTORED, fullLivesRestorationTimeInSeconds, LivesFullMessage);
            }
        }

        private void OnUserLifeRestorationsAllCanceled()
        {
            localNotificationService.Cancel(LocalNotificationsIds.ID_FULL_LIVES_RESTORED);
        }
    }
}

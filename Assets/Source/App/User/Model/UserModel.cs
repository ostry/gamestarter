﻿using System;
using System.Security;
using Claw.Modules.Models;
using GlassCrash.Game;

namespace GlassCrash.App.User
{
    public class UserModel : IPersistedModel
    {
        public event Action levelChangedSignal;
        private void NotifyLevelChanged() { if (levelChangedSignal != null) levelChangedSignal(); }
        
        public UserModel()
        {
            // We can call reset here as this model is not yet registered with persistance service and request to serialization will do nothing.
            ResetModel();
        }

        private string playerId = null;
        public string PlayerId
        {
            get { return playerId; }
            set
            {
                playerId = value;
                RequestSerialization();
            }
        }

        private int score;
        public int Score
        {
            get { return score; }
            set
            {
                score = value;
                RequestSerialization();
            }
        }

        private int level;
        public int Level
        {
            get { return level; }
            set
            {
                level = value;
                NotifyLevelChanged();
                RequestSerialization();
            }
        }

        #region IPersistedModel
        
        public event Action<IPersistedModel> serializeModelSignal;
        private void RequestSerialization()
        {
            if (serializeModelSignal != null)
                serializeModelSignal(this);
        }
        
        public void ResetModel()
        {
            if (playerId == null)
            {
                playerId = Guid.NewGuid().ToString();
            }

            score = 0;
            level = 0;

            RequestSerialization();
        }
        
        #endregion
    }
}

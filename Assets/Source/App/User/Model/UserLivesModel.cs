﻿using System;
using UnityEngine;
using Claw.Modules.Models;
using Newtonsoft.Json;
using Zenject;
using Claw.RemoteConfig;

namespace GlassCrash.App.User
{
    public class UserLivesModel : IPersistedModel
    {
        [Inject]
        IRemoteConfigService remoteConfig;

        [Inject]
        UserEvents userEvents;
        
        #region IPersistedModel
        
        public event Action<IPersistedModel> serializeModelSignal;
        private void RequestSerialization()
        {
            if (serializeModelSignal != null)
                serializeModelSignal(this);
        }
        
        #endregion
        
        public event Action livesCountChangedSignal;
        private void NotifyLivesCountChanged()
        {
            if (livesCountChangedSignal != null)
                livesCountChangedSignal();
        }

        [JsonProperty]
        int livesCount;
        
        [JsonIgnore]
        public int MaxLivesCount {
            get {
                if (remoteConfig != null)
                {
                    int maxLivesCount = remoteConfig.GetInt("max_lives_count");
                    return maxLivesCount;
                }

                return 4;
            }
        }
        
        [JsonIgnore]
        public int LivesCount {
            get  {
                if (livesCount > MaxLivesCount) livesCount = MaxLivesCount;
                return livesCount; 
            }
        }
        
        [JsonIgnore]
        public bool IsNotEnoughLives { get { return LivesCount < 1; } }
        
        [JsonIgnore]
        public bool IsLivesFull { get { return LivesCount >= MaxLivesCount; } }
        
        public UserLivesModel()
        {
            // We can call reset here as this model is not yet registered with persistance service and request to serialization will do nothing.
            ResetModel();
        }

        public void ResetModel()
        {
            livesCount = 4;

            NotifyLivesCountChanged();
            RequestSerialization();
        }
        
        public void LooseLife()
        {
            livesCount--;
            if (livesCount < 0) livesCount = 0;

            NotifyLivesCountChanged();
            RequestSerialization();
        }
        
        public void RestoreSingleLife()
        {
            livesCount++;
            if (livesCount > MaxLivesCount) livesCount = MaxLivesCount;

            NotifyLivesCountChanged();
            userEvents.NotifyUserLifeRestored();
            RequestSerialization();
        }
    }
}

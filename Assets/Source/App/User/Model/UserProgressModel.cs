﻿using System;
using Claw.Modules.Models;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace GlassCrash.App.User
{
    public class UserProgressModel : IPersistedModel
    {
        [JsonProperty]
        private Dictionary<int, int> levelToStarsMap = new Dictionary<int, int>();
        
        public int GetStarsCountForLevelId(int levelId)
        {
            if (!levelToStarsMap.ContainsKey(levelId))
            {
                return 0;
            }
            else
            {
                return levelToStarsMap[levelId];
            }
        }
        
        public void SetStarsCountForLevelId(int levelId, int starsCount)
        {
            levelToStarsMap[levelId] = starsCount;
            RequestSerialization();
        }
        
        #region IPersistedModel

        public event Action<IPersistedModel> serializeModelSignal;
        private void RequestSerialization()
        {
            if (serializeModelSignal != null)
                serializeModelSignal(this);
        }
        
        public void ResetModel()
        {
            levelToStarsMap.Clear();
            RequestSerialization();
        }

        #endregion
    }
}

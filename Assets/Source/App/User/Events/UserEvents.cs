﻿using System;

namespace GlassCrash.App.User
{
    public class UserEvents
    {
        public event Action userLifeRestoredSignal;
        public void NotifyUserLifeRestored() { if (userLifeRestoredSignal != null) userLifeRestoredSignal(); }

        public event Action userLifeRestorationScheduledSignal;
        public void NotifyUserLifeRestorationScheduled() { if (userLifeRestorationScheduledSignal != null) userLifeRestorationScheduledSignal(); }

        public event Action userLifeRestorationsAllCanceledSignal;
        public void NotifyUserLifeRestorationsAllCanceled() { if (userLifeRestorationsAllCanceledSignal != null) userLifeRestorationsAllCanceledSignal(); }
    }
}

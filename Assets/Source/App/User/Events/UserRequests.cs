﻿using System;

namespace GlassCrash.App.User
{
    public class UserRequests
    {
        public event Action lifeRestorationRequest;
        public void RequestLifeRestoration() { if (lifeRestorationRequest != null) lifeRestorationRequest(); }
        
        /// <summary>
        /// Returns next life restoration remaining time in seconds.
        /// </summary>
        public event Func<double> nextLifeRestorationRemainingTimeInSecondsDelegate;
        public double RequestNextLifeRestorationRemainingTimeInSeconds()
        {
            if (nextLifeRestorationRemainingTimeInSecondsDelegate != null)
                return nextLifeRestorationRemainingTimeInSecondsDelegate();
            
            throw new Exception("No delegate registered");
        }
    }
}

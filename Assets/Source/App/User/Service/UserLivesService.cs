﻿using System;
using Zenject;
using UnityEngine;
using Newtonsoft.Json;
using System.Collections.Generic;
using Claw.Utils;
using Claw.TimeNS;

namespace GlassCrash.App.User
{
    class TimerAlarm
    {
        public string id;
        public double timestamp;

        public TimerAlarm(double timestamp)
        {
            this.id = Guid.NewGuid().ToString();
            this.timestamp = timestamp;
        }
    }
    
    class Timer
    {
        public event Action<TimerAlarm> alarmSignal;
        private void NotifyAlarmOnSignal(TimerAlarm alarm) { if (alarmSignal != null) alarmSignal(alarm); }
        
        public event Action<Timer> timerChangedSingal;
        private void NotifyTimerChangedSingal(Timer timer) { if (timerChangedSingal != null) timerChangedSingal(timer); }
        
        public Dictionary<string, TimerAlarm> Alarms { get; set; }
        HashSet<TimerAlarm> alarmsThetWentOff = new HashSet<TimerAlarm>();

        public Timer()
        {
            Alarms = new Dictionary<string, TimerAlarm>();
        }
        
        public List<double> AlarmsIntervalsFromNowInSeconds
        {
            get
            {
                double now = TimeUtils.Timestamp();
                List<double> alarmsTimestampsInSeconds = new List<double>();
                foreach (var alarm in Alarms.Values)
                {
                    double tsis = (alarm.timestamp - now) / 1000 ;
                    alarmsTimestampsInSeconds.Add(tsis);
                }

                alarmsTimestampsInSeconds.Sort((double a, double b) => (int)(a - b));

                return alarmsTimestampsInSeconds;
            }
        }

        public void AddAlarm(TimerAlarm alarm)
        {
            Alarms[alarm.id] = alarm;

            NotifyTimerChangedSingal(this);
        }
        
        public void RemoveAlarm(TimerAlarm alarm, bool notifyTimerChanged = true)
        {
            Alarms.Remove(alarm.id);

            if (notifyTimerChanged)
                NotifyTimerChangedSingal(this);
        }
        
        public void CancellAll()
        {
            Alarms.Clear();
            
            NotifyTimerChangedSingal(this);
        }
        
        public int AlarmsCount { get { return Alarms.Count; } }

        public void Update(float dt)
        {
            if (Alarms.Count == 0)
                return;
            
            var timestampNow = TimeUtils.Timestamp();

            alarmsThetWentOff.Clear();
            
            foreach(var alarm in Alarms.Values)
            {
                if (timestampNow >= alarm.timestamp)
                {
                    alarmsThetWentOff.Add(alarm);
                }
            }

            foreach (var alarm in alarmsThetWentOff)
            {
                RemoveAlarm(alarm, false);
            }

            if (alarmsThetWentOff.Count > 0)
            {
                NotifyTimerChangedSingal(this);
            }
            
            foreach (var alarm in alarmsThetWentOff)
            {
                NotifyAlarmOnSignal(alarm);
            }
        }
        
        public string GetSerializedString()
        {
            var alarmsJson = JsonConvert.SerializeObject(Alarms);
            return alarmsJson;
        }
        
        public void DeserializeFromString(string serializedTimerString)
        {
            var alarmsJson = serializedTimerString;
            var alarms = JsonConvert.DeserializeObject<Dictionary<string, TimerAlarm>>(alarmsJson);
            if (alarms != null)
            {
                Alarms = alarms;
            }
        }
    }

    public class UserLivesService
    {
        private const string SERVICE_PREFS_KEY = "UserLivesService/Timer";

        [Inject]
        UserEvents userEvents;

        [Inject]
        UserRequests userRequests;
        
        Timer timer;
        
        private double TimestampInSecondsFromNow(double seconds)
        {
            var tsNow = TimeUtils.Timestamp();
            var ts = tsNow + seconds * 1000;
            return ts;
        }

        public int PendingLivesRestorationsCount {
            get {
                return timer.AlarmsCount;
            }
        }

        public List<double> RestorationTimesInSeconds { get { return timer.AlarmsIntervalsFromNowInSeconds; } }

        public string RestoreLifeInSeconds(double intervalInSeconds)
        {
            var timestamp = TimestampInSecondsFromNow(intervalInSeconds);
            var alarm = new TimerAlarm(timestamp);
            timer.AddAlarm(alarm);

            userEvents.NotifyUserLifeRestorationScheduled();

            return alarm.id;
        }
        
        public void Start()
        {
            timer = new Timer();
            timer.alarmSignal += OnAlarm;
            timer.timerChangedSingal += OnTimerChanged;
            
            Deserialize();
        }
        
        public void Stop()
        {
            timer.timerChangedSingal -= OnTimerChanged;
            timer.alarmSignal -= OnAlarm;
            timer = null;
        }
        
        public void Update(float dt)
        {
            if (timer != null)
            {
                timer.Update(dt);
            }
        }
        
        public double TimeRemainingForNextLifeRestorationInSeconds()
        {
            TimerAlarm minAlarm = null;
            foreach(var alarm in timer.Alarms.Values)
            {
                if (minAlarm == null) minAlarm = alarm;
                else if (minAlarm.timestamp > alarm.timestamp) minAlarm = alarm;
            }
            
            if (minAlarm == null)
            {
                return -1;
            }

            double remainingTimeInSeconds = (minAlarm.timestamp - TimeUtils.Timestamp()) / 1000.0;
            return remainingTimeInSeconds;
        }
        
        public void CancellAllScheduledLivesRestorations()
        {
            FileLogger.Log(FileLogger.LivesService, "CancellAllScheduledLivesRestorations()");

            timer.CancellAll();

            userEvents.NotifyUserLifeRestorationsAllCanceled();
        }
        
        private void OnAlarm(TimerAlarm alarm)
        {
            FileLogger.Log(FileLogger.LivesService, "OnAlarm(), alarm: " + alarm.id);

            Serialize();
            userRequests.RequestLifeRestoration();
        }
        
        private void OnTimerChanged(Timer t)
        {
            FileLogger.Log(FileLogger.LivesService, "OnTimerChanged()");

            Serialize();
        }
        
        private void Serialize()
        {
            string timerSerializedString = timer.GetSerializedString();
            FileLogger.Log(FileLogger.LivesService, "Serialize(), value: " + timerSerializedString);

            PlayerPrefs.SetString(SERVICE_PREFS_KEY, timerSerializedString);
        }
        
        private void Deserialize()
        {
            string timerSerializedString = PlayerPrefs.GetString(SERVICE_PREFS_KEY);
            FileLogger.Log(FileLogger.LivesService, "Deserialize(), value: " + timerSerializedString);

            if (!"".Equals(timerSerializedString))
                timer.DeserializeFromString(timerSerializedString);
        }
    }
}

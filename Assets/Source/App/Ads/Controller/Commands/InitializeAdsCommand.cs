﻿using System;
using Claw;
using Claw.Ads;
using Zenject;
using GlassCrash.App.Features;

namespace GlassCrash.App.Ads
{
    public class InitializeAdsCommand : Command
    {
        [Inject]
        AdRewardedService adRewardedService;

        [Inject]
        AdInterstitialService adInterstitialService;
        
        [Inject]
        AdBannerService adBannerService;
        
        [Inject]
        FeatureFlags ff;
        
        public override void Execute()
        {
            if (ff.IsAdsEnabled)
            {
                AdMobBaseService.InitializeAds();
                //adRewardedService.InitializeRewardedAd();
                //adInterstitialService.InitializeIntersitialAd();
                adBannerService.InitializeBanner();
            }
        }
    }
}

﻿using System;
using Claw.Modules.Controller;
using Zenject;
using Claw.Ads;
using Claw.Animation.Tweening;
using Claw.RemoteConfig;

namespace GlassCrash.App.Ads
{
    public class AdsController : IController
    {
        [Inject]
        Juggler appJuggler;

        [Inject]
        IRemoteConfigService remoteConfig;
        
        [Inject]
        AdRewardedService adRewardedService;
        
        [Inject]
        AdInterstitialService adInterstitialService;
        
        float LoadAdsInterval { get { return remoteConfig.GetFloat("load_ads_interval_in_seconds"); } }
        
        public void Setup()
        {
            LoadAds();
        }

        public void Teardown()
        {
        }
        
        private void ScheduleAdsLoadedCheck()
        {
            float interval = LoadAdsInterval;
            appJuggler.StartTween(new CallbackTween(LoadAds, interval));
        }
        
        private void LoadAds()
        {
            if (!adRewardedService.IsRewardedAdLoaded())
            {
                adRewardedService.RequestRewardedAd();
            }
            
            if (!adInterstitialService.IsLoaded(AdInterstitialService.Interstitial_MainMenu))
            {
                adInterstitialService.RequestIntersitial(AdInterstitialService.Interstitial_MainMenu);
            }
            
            if (!adInterstitialService.IsLoaded(AdInterstitialService.Interstitial_StartLevel))
            {
                adInterstitialService.RequestIntersitial(AdInterstitialService.Interstitial_StartLevel);
            }
            
            ScheduleAdsLoadedCheck();
        }
    }
}

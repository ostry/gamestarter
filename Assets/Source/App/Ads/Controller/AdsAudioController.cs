﻿using System;
using Claw.Modules.Controller;
using Zenject;
using Claw.Ads;
using GlassCrash.App.Audio;

namespace GlassCrash.App.Ads
{
    /// <summary>
    /// Ads audio controller.
    /// The name is built with the rule: first what controls then what is controlled.
    /// So here Ads controll Audio. I hope to keep that naming convention in the future compound controllers.
    /// </summary>
    public class AdsAudioController : IController
    {
        [Inject]
        AdInterstitialService adInterstitialService;
        
        [Inject]
        AdRewardedService adRewardedService;

        [Inject]
        AppAudioRequests appAudioRequests;

        public void Setup()
        {
            adRewardedService.onAdShown += OnAdRwardedShown;
            adRewardedService.onAdClosed += OnAdRewardedClosed;

            adInterstitialService.onAdShown += OnAdInterstitialShown;
            adInterstitialService.onAdClosed += OnAdInterstitialClosed;
        }

        public void Teardown()
        {
            adInterstitialService.onAdClosed -= OnAdInterstitialClosed;
            adInterstitialService.onAdShown -= OnAdInterstitialShown;
            
            adRewardedService.onAdClosed -= OnAdRewardedClosed;
            adRewardedService.onAdShown -= OnAdRwardedShown;
        }
        
        private void OnAdRwardedShown()
        {
            appAudioRequests.RequestPauseAudio(this.GetType().ToString()+ " Rewarded", true);
        }
        
        private void OnAdRewardedClosed()
        {
            appAudioRequests.RequestPauseAudio(this.GetType().ToString()+ " Rewarded", false);
        }
        
        private void OnAdInterstitialShown()
        {
            appAudioRequests.RequestPauseAudio(this.GetType().ToString()+ " Interstitial", true);
        }

        private void OnAdInterstitialClosed()
        {
            appAudioRequests.RequestPauseAudio(this.GetType().ToString()+ " Interstitial", false);
        }
    }
}

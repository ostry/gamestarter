﻿using System;
using Claw.Ads;
using Claw.Modules.Controller;
using Zenject;
using GlassCrash.App.Features;
using GlassCrash.App.Tracking;

namespace GlassCrash.App.Ads
{
    public class AdsTrackingController : IController
    {
        [Inject]
        FeatureFlags ff;

        [Inject]
        AppTrackingService track;
        
        [Inject]
        AdInterstitialService adInterstitialService;

        [Inject]
        AdRewardedService adRewardedService;

        [Inject]
        AdBannerService adBannerService;

        public void Setup()
        {
            adRewardedService.onAdShown += OnAdRwardedShown;
            adRewardedService.onAdClosed += OnAdRewardedClosed;
            
            adInterstitialService.onAdShown += OnAdInterstitialShown;
            adInterstitialService.onAdClosed += OnAdInterstitialClosed;

            adBannerService.onAdShown += OnAdBannerShown;
            adBannerService.onAdClosed += OnAdBannerClosed;
        }

        public void Teardown()
        {
            adBannerService.onAdClosed -= OnAdBannerClosed;
            adBannerService.onAdShown -= OnAdBannerShown;
            
            adInterstitialService.onAdClosed -= OnAdInterstitialClosed;
            adInterstitialService.onAdShown -= OnAdInterstitialShown;

            adRewardedService.onAdClosed -= OnAdRewardedClosed;
            adRewardedService.onAdShown -= OnAdRwardedShown;
        }

        private void OnAdRwardedShown()
        {
            if (ff.IsTrackingEnabled)
            {
                track.RewardedAdShown();
            }
        }

        private void OnAdRewardedClosed()
        {}

        private void OnAdInterstitialShown()
        {
            if (ff.IsTrackingEnabled)
            {
                track.InterstitialAdShown();
            }
        }

        private void OnAdInterstitialClosed()
        {}
        
        private void OnAdBannerShown()
        {
            if (ff.IsTrackingEnabled)
            {
                track.BannerAdShown();
            }
        }

        private void OnAdBannerClosed()
        {}
    }
}

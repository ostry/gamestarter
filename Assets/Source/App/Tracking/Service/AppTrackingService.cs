﻿using System;
using Zenject;
using Claw.Tracking;
using System.Collections.Generic;

namespace GlassCrash.App.Tracking
{
    public class AppTrackingService
    {
        [Inject]
        ITrackingService ts;

        [Inject]
        GameHistoryModel gameHistoryModel;

        private Dictionary<string, object> DefaultParamsPlus(Dictionary<string, object> parameters)
        {
            return parameters;
        }

        #region Ads
        
        public void RewardedAdShown()
        {
            ts.LogEvent("RewardedAdShown", DefaultParamsPlus(new Dictionary<string, object>() {}));
        }

        public void InterstitialAdShown()
        {
            ts.LogEvent("InterstitialAdShown", DefaultParamsPlus(new Dictionary<string, object>() { }));
        }

        public void InterstitialAdAwarded()
        {
            ts.LogEvent("InterstitialAdAwarded", DefaultParamsPlus(new Dictionary<string, object>() { }));
        }
        
        public void BannerAdShown()
        {
            ts.LogEvent("BannerAdShown", DefaultParamsPlus(new Dictionary<string, object>() { }));
        }
        
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace GlassCrash.App
{
    public enum GameResult
    {
        WHO_KNOWS,
        GAME_WON,
        GAME_LOST,
        GAME_QUIT
    }
    
    public class GameResultData
    {
        public Guid gameId = Guid.NewGuid();
        public int levelId;
        public GameResult gameResult = GameResult.WHO_KNOWS;
        public int score;
        public bool gameUnlockedNewLevel = false;
        public bool levelRetryRequested = false;

        public GameResultData()
        {}
        
        public GameResultData(GameResultData grd)
        {
            gameId = grd.gameId;
            levelId = grd.levelId;
            gameResult = grd.gameResult;
            score = grd.score;
            gameUnlockedNewLevel = grd.gameUnlockedNewLevel;
            levelRetryRequested = grd.levelRetryRequested;
        }
    }
    
    public class GameHistoryModel
    {
        List<GameResultData> gamesHistory = new List<GameResultData>();
        
        public void AddGameResult(GameResultData gameResultData)
        {
            gamesHistory.Add(gameResultData);
        }
        
        public GameResultData LastGameResultData { get { return gamesHistory.Count > 0 ? gamesHistory.Last() : null; } }
    }
}

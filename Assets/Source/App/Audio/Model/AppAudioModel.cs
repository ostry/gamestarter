﻿using System;
using Claw.Modules.Models;
using System.Collections.Generic;

namespace GlassCrash.App.Audio
{
    public class AppAudioModel : IPersistedModel
    {
        public event Action<IPersistedModel> serializeModelSignal;
        private void RequestSerialization() { if (serializeModelSignal != null) serializeModelSignal(this); }

        private HashSet<string> pauseAudioRequestIds = new HashSet<string>();
        public void PauseAudio(string requestId)
        {
            pauseAudioRequestIds.Add(requestId);
        }
        
        public void ResumeAudio(string requestId)
        {
            pauseAudioRequestIds.Remove(requestId);
        }
        
        public bool IsAudioPaused { get { return pauseAudioRequestIds.Count > 0; } }
        
        private bool isFx = true;
        public bool IsFXEnabled
        {
            get { return isFx; }
            set
            {
                isFx = value;
                RequestSerialization();
            }
        }
        
        private bool isMusic = true;
        public bool IsMusicEnabled
        {
            get { return isMusic; }
            set
            {
                isMusic = value;
                RequestSerialization();
            }
        }

        public void ResetModel()
        {
            pauseAudioRequestIds.Clear();
            
            isFx = true;
            isMusic = true;
            
            RequestSerialization();
        }
    }
}

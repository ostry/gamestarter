﻿using System;
using UnityEngine;
using Zenject;

namespace GlassCrash.App.Audio
{
    public class AppAudioSource : MonoBehaviour
    {
        [Inject]
        AppAudioModel appAudioModel;

        AudioSource fxAudioSource;
        AudioSource musicAudioSource;

        public void Awake()
        {
            fxAudioSource = gameObject.AddComponent<AudioSource>();
            fxAudioSource.playOnAwake = false;
            fxAudioSource.volume = 0.5f;

            musicAudioSource = gameObject.AddComponent<AudioSource>();
            musicAudioSource.playOnAwake = false;
            musicAudioSource.volume = 0.1f;
        }

        #region Music
        
        public void PlayFX(AudioClip clip)
        {
            if (appAudioModel.IsAudioPaused ||
                !appAudioModel.IsFXEnabled)
                return;

            fxAudioSource.PlayOneShot(clip);
        }
        
        public void StopFX()
        {
            fxAudioSource.Stop();
        }
        
        public void PauseFx()
        {
            fxAudioSource.Pause();
        }
        
        public void UnPauseFx()
        {
            fxAudioSource.UnPause();
        }

        #endregion
        
        
        
        #region Music

        public void PlayMusic(AudioClip clip)
        {
            if (appAudioModel.IsAudioPaused ||
                !appAudioModel.IsMusicEnabled)
                return;

            // Do not restart if already playing given audio clip
            if (musicAudioSource.isPlaying &&
                clip.Equals(musicAudioSource.clip))
                return;
            
            musicAudioSource.loop = true;
            musicAudioSource.clip = clip;
            musicAudioSource.Play();
        }

        public void StopMusic()
        {
            musicAudioSource.Stop();
        }
        
        public void PauseMusic()
        {
            musicAudioSource.Pause();
        }

        public void UnPauseMusic()
        {
            musicAudioSource.UnPause();
        }

        #endregion
    }
}

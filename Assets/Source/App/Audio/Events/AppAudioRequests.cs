﻿using System;
namespace GlassCrash.App.Audio
{
    public class AppAudioRequests
    {
        public event Action<string, bool> pauseAudioRequest;
        public void RequestPauseAudio(string requestId, bool pause)
        {
            if (pauseAudioRequest != null)
                pauseAudioRequest(requestId, pause);
        }

        public event Action<bool> enableFxAudioRequest;
        public void RequestEnableFxAudio(bool enableFx)
        {
            if (enableFxAudioRequest != null)
                enableFxAudioRequest(enableFx);
        }

        public event Action<bool> enableMusicAudioRequest;
        public void RequestEnableMusicAudio(bool enableMusic)
        {
            if (enableMusicAudioRequest != null)
                enableMusicAudioRequest(enableMusic);
        }

        #region FX
        
        public event Action<string> playFXRequest;
        public void RequestPlayFX(string audioClipId)
        {
            if (playFXRequest != null)
                playFXRequest(audioClipId);
        }
        
        #endregion

        #region Music

        public event Action<string> playMusicRequest;
        public void RequestPlayMusic(string audioClipId)
        {
            if (playMusicRequest != null)
                playMusicRequest(audioClipId);
        }

        public event Action<string> stopMusicRequest;
        public void RequestStopMusic(string audioClipId)
        {
            if (stopMusicRequest != null)
                stopMusicRequest(audioClipId);
        }
        
        #endregion
    }
}

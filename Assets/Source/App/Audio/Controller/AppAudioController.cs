﻿using System;
using Claw.Modules.Controller;
using Zenject;
using Claw.UI.Audio;
using UnityEngine;
using Claw.UI.Dialogs;
using Claw.Utils;
using Claw.UI;
using GlassCrash.Game.Audio;
using Claw.Algorithm.Stack;

namespace GlassCrash.App.Audio
{
    public class AppAudioController : IController
    {
        [Inject]
        AppRequests appRequests;
        
        [Inject]
        AppAudioRequests appAudioRequests;
        
        [Inject]
        AudioClipsManager appAudioClipsManager;
        
        [Inject]
        GameAudioClipsManager gameAudioClipsManager;

        [Inject]
        AppAudioSource audioSource;
        
        [Inject]
        AppAudioModel appAudioModel;

        [Inject]
        DialogManagerEvents dialogManagerEvents;
        
        ListStack<string> musicIdsStack = new ListStack<string>();
        
        public void Setup()
        {
            RegisterAppAudioRequets();
            RegisterDialogsManagerEvents();
        }

        public void Teardown()
        {
            UnregisterDialogsManagerEvents();
            UnregisterAppAudioRequets();
        }

        #region App Audio Requests

        private void RegisterAppAudioRequets()
        {
            appAudioRequests.pauseAudioRequest += PauseAudio;
            appAudioRequests.enableFxAudioRequest += EnableFx;
            appAudioRequests.enableMusicAudioRequest += EnableMusic;
            
            appAudioRequests.playMusicRequest += PlayMusic;
            appAudioRequests.stopMusicRequest += StopMusic;
            
            appAudioRequests.playFXRequest += PlayFX;
        }
        
        private void UnregisterAppAudioRequets()
        {
            appAudioRequests.stopMusicRequest -= StopMusic;
            appAudioRequests.playMusicRequest -= PlayMusic;
            
            appAudioRequests.pauseAudioRequest -= PauseAudio;
            appAudioRequests.enableMusicAudioRequest -= EnableMusic;
            appAudioRequests.enableFxAudioRequest -= EnableFx;
        }
        
        private void PauseAudio(string requestId, bool pause)
        {
            bool wasAudioPaused = appAudioModel.IsAudioPaused;
            if (pause)
            {
                appAudioModel.PauseAudio(requestId);
            }
            else
            {
                appAudioModel.ResumeAudio(requestId);
            }
            
            if (appAudioModel.IsAudioPaused && !wasAudioPaused)
            {
                audioSource.PauseFx();
                audioSource.PauseMusic();
            }
            else if (!appAudioModel.IsAudioPaused && wasAudioPaused)
            {
                audioSource.UnPauseFx();
                audioSource.UnPauseMusic();
            }
        }
        
        private void EnableFx(bool value)
        {
            appAudioModel.IsFXEnabled = value;
        }
        
        private void EnableMusic(bool value)
        {
            appAudioModel.IsMusicEnabled = value;
            
            if (value && !musicIdsStack.IsEmpty())
            {
                var clipId = musicIdsStack.Peek();
                var clip = ResolveAudioClip(clipId);
                audioSource.PlayMusic(clip);
            }
            else
            {
                audioSource.StopMusic();
            }
        }
        
        private void PlayMusic(string audioClipId)
        {
            musicIdsStack.Push(audioClipId);
            
            var clip = ResolveAudioClip(audioClipId);
            audioSource.PlayMusic(clip);
        }
        
        private void StopMusic(string audioClipId)
        {
            musicIdsStack.Remove(audioClipId);
            
            audioSource.StopMusic();
        }
        
        private void PlayFX(string audioClipId)
        {
            var clip = ResolveAudioClip(audioClipId);
            audioSource.PlayFX(clip);
        }

        #endregion
        
        
        
        #region Dialogs Manager Audio
        
        private void RegisterDialogsManagerEvents()
        {
            dialogManagerEvents.dialogWillBePresented += PlayShowSound;
            dialogManagerEvents.dialogWillBeClosed += PlayHideSound;
        }
        
        private void UnregisterDialogsManagerEvents()
        {
            dialogManagerEvents.dialogWillBeClosed -= PlayHideSound;
            dialogManagerEvents.dialogWillBePresented -= PlayShowSound;
        }
        
        private void PlayShowSound(MonoView view)
        {
            var clip = ResolveAudioClip(AudioClipsManager.DialogShowSound);
            audioSource.PlayFX(clip);
        }
        
        private void PlayHideSound(MonoView view)
        {
            var clip = ResolveAudioClip(AudioClipsManager.DialogHideSound);
            audioSource.PlayFX(clip);
        }
        
        private AudioClip ResolveAudioClip(string audioClipId)
        {
            if (audioClipId == null || "".Equals(audioClipId))
                return null;
            
            var clip = appAudioClipsManager.GetAudioClipByName(audioClipId);
            if (clip != null)
                return clip;

            clip = gameAudioClipsManager.GetAudioClipByName(audioClipId);
            return clip;
        }
        
        #endregion
    }
}

﻿using System;
using Claw;
using Claw.Modules.Services;
using Zenject;
using GlassCrash.App.Audio;

namespace GlassCrash.App.Audio
{
    public class EnableFxAudioDebugCommand : Command
    {
        [Inject]
        AppAudioModel appAudioModel;

        public override void Execute()
        {
            appAudioModel.IsFXEnabled = true;
        }
    }
}

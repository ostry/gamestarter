﻿using System;
using Claw;
using Zenject;

namespace GlassCrash.App.Audio
{
    public class EnableFxSoundsCommand : Command
    {
        [Inject]
        AppAudioRequests appAudioRequests;

        public override void Execute()
        {
            appAudioRequests.RequestEnableFxAudio(true);
        }
    }
}

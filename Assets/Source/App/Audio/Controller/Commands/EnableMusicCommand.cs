﻿using System;
using Claw;
using Zenject;

namespace GlassCrash.App.Audio
{
    public class EnableMusicCommand : Command
    {
        [Inject]
        AppAudioRequests appAudioRequests;
        
        public override void Execute()
        {
            appAudioRequests.RequestEnableMusicAudio(true);
        }
    }
}

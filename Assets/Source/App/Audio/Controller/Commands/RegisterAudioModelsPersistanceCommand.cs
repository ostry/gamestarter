﻿using System;
using Claw;
using Claw.Modules.Services;
using Zenject;

namespace GlassCrash.App.Audio
{
    public class RegisterAudioModelsPersistanceCommand : Command
    {
        [Inject]
        ModelPersistanceService modelPersistanceService;

        [Inject]
        AppAudioModel appAudioModel;

        public override void Execute()
        {
            modelPersistanceService.Register(appAudioModel);
        }
    }
}

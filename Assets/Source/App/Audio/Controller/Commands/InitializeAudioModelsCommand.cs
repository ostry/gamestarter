﻿using System;
using Claw;
using Claw.Modules.Services;
using Zenject;

namespace GlassCrash.App.Audio
{
    public class InitializeAudioModelsCommand : Command
    {
        [Inject]
        ModelPersistanceService modelPersistanceService;

        [Inject]
        AppAudioModel appAudioModel;

        public override void Execute()
        {
            if (!modelPersistanceService.DeserializeModel(appAudioModel))
            {
                appAudioModel.ResetModel();
            }
        }
    }
}

﻿using GlassCrash.MainMenu;
using GlassCrash.Game;
using UnityEngine;
using Zenject;
using GlassCrash.DebugStuff;
using Claw.AppInput;
using Claw.RemoteConfig;

namespace GlassCrash.App
{
    public class AppUpdater : MonoBehaviour
    {
        [Inject]
        TouchAdapter touchAdapter;

        [Inject]
        IRemoteConfigService remoteConfig;

        [Inject]
        AppEvents appEvents;

        private DebugContext debugContext;
        private AppContext appContext;
        private MainMenuContext mainMenuContext;
        private GameContext gameContext;

        [Range(0.001f, 100.0f)]
        public float timeScale = 1.0f;
        private float lastTimeScale = -1.0f;
        private float originalFixedDeltaTime = 0.2f;

        [Range(1,120)]
        public int targetFrameRate = 60;
        private int lastTargetFrameRate = 60;
        
        public void Awake()
        {
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = 60;

            originalFixedDeltaTime = Time.fixedDeltaTime;
        }

        [Inject]
        public void Initialize(DebugContext debugContext,
                               AppContext appContext,
                               MainMenuContext mainMenuContext,
                               GameContext gameContext)
        {
            this.debugContext = debugContext;
            this.appContext = appContext;
            this.mainMenuContext = mainMenuContext;
            this.gameContext = gameContext;

            Setup();
        }

        private void OnDisable()
        {
            Teardown();
        }

        private void Setup()
        {
            appContext.Setup();

            timeScale = remoteConfig.GetFloat("time_scale");
        }
        
        private void Teardown()
        {
            appContext.Teardown();
        }

        public void Update()
        {
            if (targetFrameRate != lastTargetFrameRate)
            {
                Application.targetFrameRate = targetFrameRate;
                lastTargetFrameRate = targetFrameRate;
            }
            
            if (timeScale != lastTimeScale)
            {
                Time.timeScale = timeScale;
                Time.fixedDeltaTime = originalFixedDeltaTime * timeScale;
                lastTimeScale = timeScale;
                
                touchAdapter.ListenToManyTouchEvents();
            }
            
            //float dt = Time.deltaTime * timeScale;
            float dt = Time.deltaTime;
            
            debugContext.UpdateModule(dt);
            appContext.UpdateModule(dt);
            mainMenuContext.UpdateModule(dt);
            gameContext.UpdateModule(dt);
        }

        public void OnApplicationPause(bool pause)
        {
            appEvents.NotifyApplicationPaused(pause);
        }
    }
}

﻿using System;
using UnityEngine;
using UnityEditor;

//// Custom Editor using SerializedProperties.
//// Automatic handling of multi-object editing, undo, and prefab overrides.
//[CustomEditor(typeof(BoardItem))]
//[CanEditMultipleObjects]
//public class BoardItemInspector : Editor
//{
//    SerializedProperty typeProp;
//    SerializedProperty boardPositionProp;
//    
//    public BoardItemInspector ()
//    {
//    }
//    
//    void OnEnable () {
//        // Setup the SerializedProperties.
//        typeProp = serializedObject.FindProperty ("type");
//        boardPositionProp = serializedObject.FindProperty ("boardPosition");
//    }
//    
//    public override void OnInspectorGUI() {
////        // Update the serializedProperty - always do this in the beginning of OnInspectorGUI.
////        serializedObject.Update ();
////
////        // Show the custom GUI controls.
////        EditorGUILayout.
////
////        // Only show the damage progress bar if all the objects have the same damage value:
////        if (!damageProp.hasMultipleDifferentValues)
////            ProgressBar (damageProp.intValue / 100.0f, "Damage");
////
////        EditorGUILayout.IntSlider (armorProp, 0, 100, new GUIContent ("Armor"));
////
////        // Only show the armor progress bar if all the objects have the same armor value:
////        if (!armorProp.hasMultipleDifferentValues)
////            ProgressBar (armorProp.intValue / 100.0f, "Armor");
////
////        EditorGUILayout.PropertyField (gunProp, new GUIContent ("Gun Object"));
////
////        // Apply changes to the serializedProperty - always do this in the end of OnInspectorGUI.
////        serializedObject.ApplyModifiedProperties ();
//    }
//
//    // Custom GUILayout progress bar.
//    void ProgressBar (float value, string label) {
//        // Get a rect for the progress bar using the same margins as a textfield:
//        Rect rect = GUILayoutUtility.GetRect (18, 18, "TextField");
//        EditorGUI.ProgressBar (rect, value, label);
//        EditorGUILayout.Space ();
//    }
//}

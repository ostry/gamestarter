﻿using System;
using Claw.UI;

namespace GlassCrash.MainMenu
{
    public class MainMenuView : MonoView
    {
        public event Action playSignal;
        public event Action settingsSignal;
        public event Action creditsSignal;

        public override void Setup()
        {
        }

        public override void Deactivate()
        {
        }
        
        public void OnPlay()
        {
            if (playSignal != null)
            {
                playSignal();
            }
        }
        
        public void OnSettings()
        {
            if (settingsSignal != null)
            {
                settingsSignal();
            }
        }
        
        public void OnCredits()
        {
            if (creditsSignal != null)
            {
                creditsSignal();
            }
        }
    }
    
}
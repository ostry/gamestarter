﻿using System;
using UnityEngine;
using Claw;
using Zenject;
using Claw.UI;
using GlassCrash.Game;
using GlassCrash.App;
using Claw.UnityUtils;

namespace GlassCrash.MainMenu
{
    public class MainMenuViewController : ViewController
    {
        private MainMenuView mainMenuView;
        private CommandsQueue commandsQueue;

        private MainMenuView view;

        [Inject]
        public void Initialize(CommandsQueue commandsQueue)
        {
            this.commandsQueue = commandsQueue;
        }

        public override void SetView(MonoView monoView)
        {
            view = monoView as MainMenuView;
            view.playSignal += OnPlay;
            view.settingsSignal += OnSettings;
            view.creditsSignal += OnCredits;
        }

        public override void Teardown()
        {
            view.creditsSignal -= OnCredits;
            view.settingsSignal -= OnSettings;
            view.playSignal -= OnPlay;
        }
        
        private void OnPlay()
        {
            commandsQueue.Enqueue<PresentPlayLevelDialogCommand>();
        }
        
        private void OnSettings()
        {
            commandsQueue.Enqueue<PresentSettingsDialogCommand>();
        }
        
        private void OnCredits()
        {
            commandsQueue.Enqueue<PresentCreditsDialogCommand>();
        }
    }
}
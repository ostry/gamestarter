﻿using Claw.UI;
using Zenject;
using Claw.Modules;
using GlassCrash.DebugStuff;
using Claw;
using GlassCrash.MainMenu.Lives;
using Claw.Modules.Controller;
using UnityEngine;
using Claw.UI.Dialogs;
using GlassCrash.MainMenu.Dialogs;

namespace GlassCrash.MainMenu
{
    public class MainMenuContext : MonoBehaviour, IModule
    {
        [Inject]
        DebugCommandsModel debugModel;
        
        [Inject]
        CommandsQueue commandsQueue;
        
        [Inject]
        ControllersFactory controllersFactory;
        
        MainMenuController mainMenuController;
        
        public void Dispose() {}

        [Inject]
        ViewsResolver mainMenuViewsResolver;

        [Inject]
        private void MapViews(ViewsManager viewsManager)
        {
            viewsManager.Map<MainMenuView, MainMenuViewController>();

            viewsManager.Map<DialogsContainerView, DialogsContainerViewController>();

            // PlayLevelDialog
            viewsManager.Map<PlayLevelDialog, PlayLevelDialogController>();

            // OutOfLivesDialog
            viewsManager.Map<OutOfLivesDialog, OutOfLivesAppDialogController>();

            // AddLivesDialog
            viewsManager.Map<AddLivesDialog, AddLivesDialogController>();

            // SettingsDialog
            viewsManager.Map<SettingsDialog, SettingsDialogController>();

            // CreditsDialog
            viewsManager.Map<CreditsDialog, CreditsDialogController>();

            // Lives
            viewsManager.Map<LivesView, LivesViewController>();
            viewsManager.Map<LivesTimerView, LivesTimerViewController>();
        }
        
        public void Setup()
        {
            RegisterDebugCommands();
            mainMenuController = controllersFactory.CreateController<MainMenuController>();
            mainMenuController.Setup();

            mainMenuViewsResolver.Resolve<DialogsContainerView>().SetActive(true);
        }
        
        public void Teardown()
        {
            mainMenuController.Teardown();
            debugModel.UnregisterModule(this);
        }
        
        public void UpdateModule(float dt)
        {
            commandsQueue.run();
            if (mainMenuController != null)
            {
                mainMenuController.Update(dt);
            }
        }

        private void RegisterDebugCommands()
        {
            debugModel.RegisterCommand(this, new CommandExecution(typeof(ReloadMainMenuDebugCommand), commandsQueue));

            // Lives
            debugModel.RegisterCommand(this, new CommandExecution(typeof(LooseLifeDebugComnand), commandsQueue));
            debugModel.RegisterCommand(this, new CommandExecution(typeof(LooseAllLifesDebugComnand), commandsQueue));
            debugModel.RegisterCommand(this, new CommandExecution(typeof(RestoreLifeDebugCommand), commandsQueue));
            debugModel.RegisterCommand(this, new CommandExecution(typeof(RestoreLifeInSecondsDebugCommand), commandsQueue));
            debugModel.RegisterCommand(this, new CommandExecution(typeof(PresentOutOfLivesDialogDebugCommand), commandsQueue));
        }
    }
}

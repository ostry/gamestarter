﻿using UnityEngine;
using Zenject;
using ModestTree;
using Claw;
using Claw.UI;
using Claw.UI.Dialogs;
using Claw.Animation.Tweening;
using Claw.Modules.Controller;
using Claw.UnityUtils;
using GlassCrash.MainMenu.Tracking;
using GlassCrash.MainMenu.Events;

namespace GlassCrash.Game
{
    public class MainMenuInstaller : MonoInstaller
    {
        IGameObjectResolver mainMenuResovler;
        
        private void BindUp<T>()
            where T : class
        {
            var instance = Container.TryResolve<T>();
            if (!Container.IsValidating)
            {
                Assert.IsNotNull(instance);
            }
            
            Container.ParentContainers.ForEach(delegate (DiContainer parentContainer){
                parentContainer.Bind<T>().FromInstance(instance).AsSingle();    
            });
        }
        
        public override void InstallBindings()
        {
            InstallCore();
            InstallMainMenuTracking();
            InstallMainMenu();
        }
        
        private void InstallCore()
        {
            var cameraOrientationObserver = GameObject.Find("Main Camera").GetComponent<CameraOrientationObserver>();
            Container.Bind<CameraOrientationObserver>().FromInstance(cameraOrientationObserver);
            
            mainMenuResovler = new GameObjectResolver(gameObject);
            Container.Bind<IGameObjectResolver>().FromInstance(mainMenuResovler);

            //Controller
            Container.Bind<ControllersFactory>().AsSingle();
            
            // Commands
            Container.Bind<CommandsQueue>().AsSingle().WithArguments("MainMenu");
            Container.Bind<CommandTweenFactory>().AsSingle();
            
            // Views
            Container.Bind<ViewsManager>().AsSingle();
            Container.Bind<ViewsResolver>().AsSingle().WithArguments(mainMenuResovler);
            
            // Dialogs
            Container.Bind<DialogsManager>().AsSingle();

            // Game Finite State Machine
            Container.Bind<StateFactory>().AsSingle();
        }

        private void InstallMainMenu()
        {
            Container.Bind<MainMenuRequests>().AsSingle();
        }

        private void InstallMainMenuTracking()
        {
            Container.Bind<MainMenuTrackingService>().AsSingle();
        }
    }
}

﻿using Claw;
using Claw.FiniteStateMachine;
using Claw.UI.Dialogs;
using Zenject;
using GlassCrash.App.User;
using GlassCrash.App.Features;
using GlassCrash.MainMenu.Tracking;
using GlassCrash.MainMenu.Dialogs;

namespace GlassCrash.MainMenu
{
    public class PlayLevelDialogState : BaseState
    {
        [Inject]
        FeatureFlags ff;

        [Inject]
        MainMenuTrackingService track;
        
        [Inject]
        UserLivesModel userLivesModel;
        
        DialogsManager dialogsManager;
        CommandsQueue commandsQueue;

        public PlayLevelDialogState(DialogsManager dialogsManager,
                                    CommandsQueue commandsQueue)
        {
            this.dialogsManager = dialogsManager;
            this.commandsQueue = commandsQueue;
        }

        public override void Enter()
        {
            if (userLivesModel.IsNotEnoughLives)
            {
                commandsQueue.Enqueue<GoMainMenu<OutOfLivesDialogState>>();
            }
            else
            {
                PresentPlayLevelDialog();
            }
        }

        public override void Exit()
        {
        }

        public override void Update(float dt)
        {
        }
        
        private void PresentPlayLevelDialog()
        {
            if (ff.IsTrackingEnabled)
            {
                track.PlayLevelDialog();
            }
            
            dialogsManager.PresentDialog<PlayLevelDialog>(
                    new DialogAction<PlayLevelDialogStateCommands.ApproveCommand>(),
                    new DialogAction<PlayLevelDialogStateCommands.CancelCommand>());
        }
    }

    namespace PlayLevelDialogStateCommands
    {
        class ApproveCommand : Command
        {
            [Inject]
            CommandsQueue commandsQueue;

            public override void Execute()
            {
                commandsQueue.Enqueue<GoMainMenu<MainMenuExitState>>();
            }
        }

        class CancelCommand : Command
        {
            [Inject]
            CommandsQueue commandsQueue;

            public override void Execute()
            {
                commandsQueue.Enqueue<GoMainMenu<LevelSelectionState>>();
            }
        }
    }
    

}

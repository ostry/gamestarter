﻿using System;
using Claw;
using System.Runtime.CompilerServices;
using Claw.UI;
using Claw.FiniteStateMachine;
using GlassCrash.App;
using Zenject;
using GlassCrash.App.Features;
using GlassCrash.MainMenu.Tracking;

namespace GlassCrash.MainMenu
{
    public class MainMenuExitState : BaseState
    {
        [Inject]
        FeatureFlags ff;

        [Inject]
        MainMenuTrackingService track;
        
        [Inject]
        AppRequests appRequests;

        [Inject]
        CommandsQueue commandsQueue;
        
        private readonly LoadingModel loadingModel;
        private readonly MainMenuView mainManuView;
        
        public MainMenuExitState(ViewsResolver viewsResolver,
                                 LoadingModel loadingModel)
        {
            this.loadingModel = loadingModel;
            this.mainManuView = viewsResolver.Resolve<MainMenuView>();
        }

        public override void Enter()
        {
            if (ff.IsTrackingEnabled)
            {
                track.MainMenuInit();
            }
            
            appRequests.RequestPlayLevel(() => {
                mainManuView.SetActive(false);
            });
        }

        public override void Exit()
        {
        }

        public override void Update(float dt)
        {
        }
    }
}

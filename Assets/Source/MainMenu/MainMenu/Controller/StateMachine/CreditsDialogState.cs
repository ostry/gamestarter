﻿using System;
using Claw;
using Claw.FiniteStateMachine;
using Claw.UI;
using Claw.UI.Dialogs;
using Zenject;
using GlassCrash.MainMenu;
using GlassCrash.App;
using GlassCrash.App.User;
using GlassCrash.MainMenu.Events;
using GlassCrash.MainMenu.Dialogs;

namespace GlassCrash.MainMenu
{
    public class CreditsDialogState : BaseState
    {
        DialogsManager dialogsManager;
        CommandsQueue commandsQueue;

        public CreditsDialogState(DialogsManager dialogsManager,
                                    CommandsQueue commandsQueue)
        {
            this.dialogsManager = dialogsManager;
            this.commandsQueue = commandsQueue;
        }

        public override void Enter()
        {
            dialogsManager.PresentDialog<CreditsDialog>(
                new DialogAction<CreditsStateCommands.ApproveCommand>(),
                new DialogAction<CreditsStateCommands.CancelCommand>());
        }

        public override void Exit()
        {
        }

        public override void Update(float dt)
        {
        }
    }

    namespace CreditsStateCommands
    {
        class ApproveCommand : Command
        {
            [Inject]
            MainMenuRequests mainMenuRequests;

            public override void Execute()
            {
                mainMenuRequests.RequestSetState<LevelSelectionState>();
            }
        }

        class CancelCommand : Command
        {
            [Inject]
            MainMenuRequests mainMenuRequests;

            public override void Execute()
            {
                mainMenuRequests.RequestSetState<LevelSelectionState>();
            }
        }
    }
    

}

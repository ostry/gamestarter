﻿using System;
using Claw;
using Claw.FiniteStateMachine;
using Claw.UI;
using Claw.UI.Dialogs;
using Zenject;
using GlassCrash.MainMenu.Dialogs;

namespace GlassCrash.MainMenu
{
    public class AddLivesState : BaseState
    {
        DialogsManager dialogsManager;
        CommandsQueue commandsQueue;

        public AddLivesState(DialogsManager dialogsManager,
                             CommandsQueue commandsQueue)
        {
            this.dialogsManager = dialogsManager;
            this.commandsQueue = commandsQueue;
        }

        public override void Enter()
        {
            dialogsManager.PresentDialog<AddLivesDialog>(
                new DialogAction<OutOfLivesDialogStateCommands.EmptyCommand>(),
                new DialogAction<OutOfLivesDialogStateCommands.CancelCommand>());
        }

        public override void Exit()
        {
        }

        public override void Update(float dt)
        {
        }
    }

    namespace AddLivesStateStateCommands
    {
        class EmptyCommand : Command
        {
            public override void Execute() { }
        }

        class CancelCommand : Command
        {
            [Inject]
            CommandsQueue commandsQueue;

            public override void Execute()
            {
                commandsQueue.Enqueue<GoMainMenu<LevelSelectionState>>();
            }
        }
    }
}

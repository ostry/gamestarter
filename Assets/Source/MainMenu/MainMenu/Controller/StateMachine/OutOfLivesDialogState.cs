﻿using System;
using Claw;
using Claw.FiniteStateMachine;
using Claw.UI;
using Claw.UI.Dialogs;
using Zenject;
using GlassCrash.App.Features;
using GlassCrash.MainMenu.Tracking;
using GlassCrash.MainMenu.Dialogs;

namespace GlassCrash.MainMenu
{
    public class OutOfLivesDialogState : BaseState
    {
        [Inject]
        FeatureFlags ff;

        [Inject]
        MainMenuTrackingService track;
        
        DialogsManager dialogsManager;
        CommandsQueue commandsQueue;

        public OutOfLivesDialogState(DialogsManager dialogsManager,
                                    CommandsQueue commandsQueue)
        {
            this.dialogsManager = dialogsManager;
            this.commandsQueue = commandsQueue;
        }

        public override void Enter()
        {
            if (ff.IsTrackingEnabled)
            {
                track.SagaOutOfLives();
            }
            
            dialogsManager.PresentDialog<OutOfLivesDialog>(
                new DialogAction<OutOfLivesDialogStateCommands.EmptyCommand>(),
                new DialogAction<OutOfLivesDialogStateCommands.CancelCommand>());
        }

        public override void Exit()
        {
        }

        public override void Update(float dt)
        {
        }
    }

    namespace OutOfLivesDialogStateCommands 
    {
        class EmptyCommand : Command
        {
            public override void Execute() { }
        }

        class CancelCommand : Command
        {
            [Inject]
            CommandsQueue commandsQueue;

            public override void Execute()
            {
                commandsQueue.Enqueue<GoMainMenu<LevelSelectionState>>();
            }
        }
    }
}

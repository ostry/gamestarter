﻿using Claw.FiniteStateMachine;
using Claw.UI;
using GlassCrash.App;
using Zenject;
using GlassCrash.App.User;
using GlassCrash.App.Features;
using GlassCrash.MainMenu.Tracking;
using Claw.Ads;
using Claw;
using GlassCrash.App.Modules;

namespace GlassCrash.MainMenu
{
    public class MainMenuInitialState : AnimatedState
    {
        [Inject]
        FeatureFlags ff;

        [Inject]
        MainMenuTrackingService track;

        [Inject]
        AdPresenterProvider adPresenterProvider;

        [Inject]
        AppRequests appRequests;

        [Inject]
        AppModulesModel appModulesModel;

        [Inject]
        UserModel userModel;

        [Inject]
        GameHistoryModel gameHistoryModel;

        MainMenuView mainManuView;

        private IAdPresenter InterstialPresenter { get { return adPresenterProvider.GetAdPresenter(AdInterstitialService.Interstitial_MainMenu); } }

        public MainMenuInitialState(ViewsResolver viewsResolver)
        {
            this.mainManuView = viewsResolver.Resolve<MainMenuView>();
        }

        public override void Enter()
        {
            if (ff.IsTrackingEnabled)
            {
                track.MainMenuInit();
            }

            mainManuView.SetActive(true);

            if (ff.IsMainMenuInterstitialEnabled)
            {
                InterstialPresenter.PresentAd();
            }

            if (appModulesModel.IsFirstTimeInMainMenu)
            {
                Enqueue<GoMainMenu<WarmupMainMenuState>>();
            }
            else
            {
                appRequests.RequestHideLoadingView();
                Enqueue<GoMainMenu<LevelSelectionState>>();
            }
        }
    }
}

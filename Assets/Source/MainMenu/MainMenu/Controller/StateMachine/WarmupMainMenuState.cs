﻿using Claw.FiniteStateMachine;
using Claw.UI.Dialogs;
using Zenject;
using Claw.UI;
using UnityEngine;
using GlassCrash.App.Audio;
using GlassCrash.MainMenu.Dialogs;
using GlassCrash.App;

namespace GlassCrash.MainMenu
{
    public class WarmupMainMenuState : AnimatedState
    {
        [Inject]
        DialogsManager dialogsManager;

        [Inject]
        AppRequests appRequests;
        
        [Inject]
        AppAudioRequests appAudioRequests;
        
        private float timeScale;

        float enterTime;
        float exitTime;
        
        public override void Enter()
        {
            base.Enter();
            
            BeginTimeMeasuring();

            PauseAppAudio();
            SpeedUpAnimations();
            WarmupUISystem();
        }

        public override void Exit()
        {
            base.Exit();

            ResetAnimationsSpeed();
            ResumeAppAudio();
            
            EndTimeMeasuring();

            appRequests.RequestHideLoadingView();
        }
        
        private void WarmupUISystem()
        {
            var view = dialogsManager.PresentDialog<PlayLevelDialog>();
            view.viewShownSingal += HideView;
        }
        
        private void HideView(MonoView view)
        {
            view.viewShownSingal -= HideView;
            
            EnqueueManagedCallback(()=> {
                ManageAnimation(view.Hide(() => {
                    ProceedToNextState();
                }));
            });
        }

        private void ProceedToNextState()
        {
            dialogsManager.CancelAllDialogs();

            Enqueue<GoMainMenu<LevelSelectionState>>();
        }
        
        private void SpeedUpAnimations()
        {
            timeScale = Time.timeScale;
            Time.timeScale = 100.0f;
        }
        
        private void ResetAnimationsSpeed()
        {
            Time.timeScale = timeScale;
        }
        
        private void BeginTimeMeasuring()
        {
            enterTime = Time.realtimeSinceStartup;
        }
        
        private void EndTimeMeasuring()
        {
            exitTime = Time.realtimeSinceStartup;
            float warmupTime = exitTime - enterTime;
            Debug.Log("Time spent at warmup state: " + warmupTime);
        }
        
        private void PauseAppAudio()
        {
            appAudioRequests.RequestPauseAudio(this.GetType().ToString(), true);
        }
        
        private void ResumeAppAudio()
        {
            appAudioRequests.RequestPauseAudio(this.GetType().ToString(), false);
        }
    }
}

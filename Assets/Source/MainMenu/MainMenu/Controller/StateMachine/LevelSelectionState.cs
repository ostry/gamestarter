﻿using System;
using Claw;
using Claw.FiniteStateMachine;
using Claw.UI;
using UnityEngine;
using UnityEngine.Rendering;
using Zenject;
using Claw.AppInput;
using GlassCrash.App;

namespace GlassCrash.MainMenu
{
    public class LevelSelectionState : BaseState
    {
        [Inject]
        InputAdapter inputAdapter;

        [Inject]
        AppRequests appRequests;

        public override void Enter()
        {
            inputAdapter.onKeyBackDown += OnKeyBack;
        }

        public override void Exit()
        {
            inputAdapter.onKeyBackDown -= OnKeyBack;
        }

        public override void Update(float dt)
        {
        }

        private void OnKeyBack()
        {
            appRequests.RequestQuitApplication();
        }
    }
}

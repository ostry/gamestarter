﻿using Zenject;
using Claw;
using Claw.FiniteStateMachine;
using Claw.UI.Dialogs;
using GlassCrash.MainMenu.Events;
using GlassCrash.MainMenu.Dialogs;

namespace GlassCrash.MainMenu
{
    public class SettingsDialogState : BaseState
    {
        DialogsManager dialogsManager;
        CommandsQueue commandsQueue;

        public SettingsDialogState(DialogsManager dialogsManager,
                                    CommandsQueue commandsQueue)
        {
            this.dialogsManager = dialogsManager;
            this.commandsQueue = commandsQueue;
        }

        public override void Enter()
        {
            dialogsManager.PresentDialog<SettingsDialog>(
                new DialogAction<SettingsStateCommands.ApproveCommand>(),
                new DialogAction<SettingsStateCommands.CancelCommand>());
        }

        public override void Exit()
        {
        }

        public override void Update(float dt)
        {
        }
    }

    namespace SettingsStateCommands
    {
        class ApproveCommand : Command
        {
            [Inject]
            MainMenuRequests mainMenuRequests;

            public override void Execute()
            {
                mainMenuRequests.RequestSetState<LevelSelectionState>();
            }
        }

        class CancelCommand : Command
        {
            [Inject]
            MainMenuRequests mainMenuRequests;

            public override void Execute()
            {
                mainMenuRequests.RequestSetState<LevelSelectionState>();
            }
        }
    }
    

}

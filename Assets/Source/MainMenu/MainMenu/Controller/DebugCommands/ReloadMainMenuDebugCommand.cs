﻿using System;
using Claw;
using Zenject;
using GlassCrash.App;
using Claw.UI;

namespace GlassCrash.MainMenu
{
    public class ReloadMainMenuDebugCommand : Command
    {
        [Inject]
        CommandsQueue commandsQueue;

        [Inject]
        ViewsResolver viewsResolver;
        
        public override void Execute()
        {
            viewsResolver.Resolve<MainMenuView>().SetActive(false);
            commandsQueue.Enqueue<GoMainMenu<MainMenuInitialState>>();
        }
    }
}

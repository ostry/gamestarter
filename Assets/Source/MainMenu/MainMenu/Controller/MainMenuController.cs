﻿using System;
using Claw;
using Claw.FiniteStateMachine;
using GlassCrash.MainMenu;
using GlassCrash.MainMenu.Ads;
using Claw.Modules.Controller;
using Zenject;
using GlassCrash.MainMenu.Events;

namespace GlassCrash.MainMenu
{
    public class MainMenuController : IController
    {
        [Inject]
        StateFactory stateFactory;
        
        [Inject]
        ControllersFactory controllersFactory;
        
        [Inject]
        MainMenuRequests mainMenuRequests;
        
        private FiniteStateMachine fsm;
        private MainMenuAdsController mainMenuAdsController;
        
        public FiniteStateMachine StateMachine
        {
            get { return fsm; }
        }
        
        public void Setup()
        {
            InitializeFSM();
            InitializeAdsController();
            RegisterFiniteStateMachineRequestHandlers();
        }

        public void Teardown()
        {
            UnregisterFiniteStateMachineRequestHandlers();
            mainMenuAdsController.Teardown();
            fsm.Dispose();
        }
        
        public void Update(float dt)
        {
            fsm.Update(dt);
        }
        
        private void InitializeFSM()
        {
            fsm = new FiniteStateMachine(stateFactory, "Menu");
            fsm.SetState<MainMenuInitialState>();
        }

        public void InitializeAdsController()
        {
            mainMenuAdsController = controllersFactory.CreateController<MainMenuAdsController>();
            mainMenuAdsController.Setup();
        }
        
        private void RegisterFiniteStateMachineRequestHandlers()
        {
            mainMenuRequests.setStateRequest += OnFiniteStateMachine_SetState;
            mainMenuRequests.isInStateDelegate += IsInState;
        }

        private void UnregisterFiniteStateMachineRequestHandlers()
        {
            mainMenuRequests.isInStateDelegate -= IsInState;
            mainMenuRequests.setStateRequest -= OnFiniteStateMachine_SetState;
        }

        private void OnFiniteStateMachine_SetState(Type stateType)
        {
            // typeof(GoApp<>).MakeGenericType(nextStateType)
            fsm.SetState(stateType);
        }
        
        private bool IsInState(Type stateType)
        {
            return fsm.IsInState(stateType);
        }
    }
}

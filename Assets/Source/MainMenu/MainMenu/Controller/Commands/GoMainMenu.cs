﻿using System;
using Claw;
using Claw.FiniteStateMachine;
using Zenject;
using GlassCrash.MainMenu.Events;

namespace GlassCrash.MainMenu
{
    public class GoMainMenu<StateT> : Command where StateT : BaseState
    {
        [Inject]
        MainMenuRequests mainMenuRequests;

        public override void Execute()
        {
            mainMenuRequests.RequestSetState<StateT>();
        }
    }
}

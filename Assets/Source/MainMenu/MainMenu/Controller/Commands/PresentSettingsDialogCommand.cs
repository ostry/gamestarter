﻿using System;
using UnityEngine;
using Claw;
using GlassCrash;
using GlassCrash.MainMenu;
using Zenject;
using GlassCrash.MainMenu.Events;

namespace GlassCrash.MainMenu
{
    public class PresentSettingsDialogCommand : Command
    {
        [Inject]
        MainMenuRequests mainMenuRequests;
        
        public override void Execute()
        {
            //commandsQueue.Enqueue<GoMainMenu<PlayLevelDialogState>>();
            mainMenuRequests.RequestSetState<SettingsDialogState>();
        }
    }
}
﻿using System;
using UnityEngine;
using Claw;
using GlassCrash;
using GlassCrash.MainMenu;
using Zenject;
using GlassCrash.MainMenu.Events;

namespace GlassCrash.MainMenu
{
    public class PresentCreditsDialogCommand : Command
    {
        [Inject]
        MainMenuRequests mainMenuRequests;
        
        public override void Execute()
        {
            mainMenuRequests.RequestSetState<CreditsDialogState>();
        }
    }
}
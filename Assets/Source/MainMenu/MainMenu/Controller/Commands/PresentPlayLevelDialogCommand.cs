﻿using System;
using UnityEngine;
using Claw;
using GlassCrash;
using GlassCrash.MainMenu;
using Zenject;

namespace GlassCrash.MainMenu
{
    public class PresentPlayLevelDialogCommand : Command
    {
        [Inject]
        CommandsQueue commandsQueue;
        
        public override void Execute()
        {
            commandsQueue.Enqueue<GoMainMenu<PlayLevelDialogState>>();
        }
    }
}
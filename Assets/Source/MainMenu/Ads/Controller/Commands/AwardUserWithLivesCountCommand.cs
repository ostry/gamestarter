﻿using Claw;
using Zenject;
using GlassCrash.App.User;
using GlassCrash.MainMenu.Tracking;

namespace GlassCrash.MainMenu.Ads
{
    public class AwardUserWithLivesCountCommand : Command
    {
        public class Payload
        {
            public int livesCount;
            
            public Payload(int livesCount)
            {
                this.livesCount = livesCount;
            }
        }
        
        [Inject]
        UserLivesModel userLivesModel;

        [Inject]
        MainMenuTrackingService mainMenuTrackingService;
        
        public override void Execute(object payload)
        {
            var p = payload as Payload;

            //for (int i = 0; i < p.livesCount; ++i)
            for (int i = 0; i < 1; ++i)
            {
                userLivesModel.RestoreSingleLife();
            }

            int livesCount = userLivesModel.LivesCount;
            UnityEngine.Debug.Log("AwardUserWithLivesCountCommand executed. userLivesModel.LivesCount: " + livesCount);

            mainMenuTrackingService.SagaOutOfLivesAdRewardAwarded();
        }
    }
}

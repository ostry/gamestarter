﻿using System;
using Claw;
using Zenject;

namespace GlassCrash.MainMenu.Ads
{
    public class UpdateLivesCountViewAfterAdRewardCommand : Command
    {
        public class Payload
        {
            public int livesCount;

            public Payload(int livesCount)
            {
                this.livesCount = livesCount;
            }
        }
        
        //[Inject]
        //GameEvents gameEvents;

        //[Inject]
        //GameRequests gameRequests;

        public override void Execute(object payload)
        {
            var p = payload as Payload;
            
            //gameEvents.NotifyTimeToUpdateMovesCount();
            //gameRequests.RequestPresentMovesCountReward(p.movesCount);
            
            //UnityEngine.Debug.Log("AwardUserWithMovesCountCommand executed. payload.movesCount: " + p.movesCount);
            
            UnityEngine.Debug.Log("AwardUserWithLivesCountCommand executed. payload.livesCount: " + p.livesCount);
        }
    }
}

﻿using System;
using Claw;
using Claw.Ads;
using Claw.Animation.Tweening;
using Claw.Modules.Controller;
using GlassCrash.Game.Ads;
using Zenject;
using GlassCrash.MainMenu.Events;
using Claw.Tracking;
using GlassCrash.MainMenu.Tracking;

namespace GlassCrash.MainMenu.Ads
{
    public class MainMenuAdsController : IController
    {
        [Inject]
        AdRewardedService adRewardedService;
        
        [Inject]
        AdInterstitialService adInterstitialService;

        [Inject]
        CommandsQueue commandsQueue;

        [Inject]
        Juggler appJuggler;

        [Inject]
        MainMenuRequests mainMenuRequests;

        public void Setup()
        {
            adRewardedService.onAdRewarded += OnAdRewarded;
            adInterstitialService.onAdRewarded += OnAdRewarded;
        }
        
        public void Teardown()
        {
            adInterstitialService.onAdRewarded -= OnAdRewarded;
            adRewardedService.onAdRewarded -= OnAdRewarded;
        }

        private void OnAdRewarded(IAdRewardedService adService, AdReward reward)
        {
            UnityEngine.Debug.Log("MainMenuAdsController.OnAdRewarded() amount: " + reward.Amount + " type: " + reward.Type);

            int livesCount = (int)reward.Amount;
            commandsQueue.Enqueue<AwardUserWithLivesCountCommand>(new AwardUserWithLivesCountCommand.Payload(livesCount));

            adService.onAdClosedOnce += () =>
            {
                UnityEngine.Debug.Log("MainMenuAdsController adRewardedService.onAdClosedOnce()");
                commandsQueue.Enqueue<UpdateLivesCountViewAfterAdRewardCommand>(new UpdateLivesCountViewAfterAdRewardCommand.Payload(livesCount));
                
                if (mainMenuRequests.IsInState<OutOfLivesDialogState>())
                {
                    commandsQueue.Enqueue<GoMainMenu<PlayLevelDialogState>>();
                }
                else
                {
                    commandsQueue.Enqueue<GoMainMenu<LevelSelectionState>>();
                }
            };
        }
    }
}

﻿using System;
using Zenject;

namespace GlassCrash.MainMenu.Ads
{
    public class MainMenuAdsFactory
    {
        [Inject]
        DiContainer container;
        
        public MainMenuAdsController CreateMainMenuAdsController()
        {
            var instance = container.Instantiate<MainMenuAdsController>();
            return instance;
        }
    }
}

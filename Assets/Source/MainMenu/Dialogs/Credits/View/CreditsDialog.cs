﻿using System;
using Claw.UI;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace GlassCrash.MainMenu.Dialogs
{
    public class CreditsDialog : DoubleChoiceDialogView
    {
        public event Action<bool> fxToggledSignal;
        public event Action<bool> musicToggledSignal;
        
        public event Action debugToggledSignal;
        
        public override void Setup()
        {
            base.Setup();
        }

        public override void Deactivate()
        {
        }
    }
}

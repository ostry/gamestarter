﻿using System;
using Claw.UI;
using UnityEngine;
using Claw;
using Zenject;
using GlassCrash.App.Audio;
using GlassCrash.DebugStuff;

namespace GlassCrash.MainMenu.Dialogs
{
    public class CreditsDialogController : ViewController
    {
        CreditsDialog view;

        public override void SetView(MonoView monoView)
        {
            view = monoView as CreditsDialog;
        }

        public override void Teardown()
        {
        }
    }
}

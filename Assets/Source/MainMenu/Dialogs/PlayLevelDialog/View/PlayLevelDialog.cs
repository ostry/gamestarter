﻿using UnityEngine.UI;
using Claw.UI;
using GlassCrash.Game.Dialogs;

namespace GlassCrash.MainMenu.Dialogs
{
    public class PlayLevelDialog : DoubleChoiceDialogView
    {
        private Text levelTitle;

        public DialogStarsView DialogStarsView { get { return ViewsResolver.Resolve<DialogStarsView>(); } }
        
        public override void Setup()
        {
            base.Setup();
            
            levelTitle = Resolver.FindGameObjectAnywhere("Panel/LevelTitle").GetComponent<Text>();
            
            DialogStarsView.SetActive(true);
        }

        public override void Deactivate()
        {
        }

        public void SetLevelTitle(string text)
        {
            levelTitle.text = text;
        }
    }
}

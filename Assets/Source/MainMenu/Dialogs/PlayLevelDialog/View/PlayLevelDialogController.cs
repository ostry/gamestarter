﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Claw;
using Zenject;
using Claw.UI;
using Claw.UI.Dialogs;
using GlassCrash.App.User;

namespace GlassCrash.MainMenu.Dialogs
{
    public class PlayLevelDialogController : ViewController
    {
        PlayLevelDialog view;
        DialogsManager dialogsManager;

        [Inject]
        UserProgressModel userProgressModel;
        
        [Inject]
        public void Initialize(DialogsManager dialogsManager)
        {
            this.dialogsManager = dialogsManager;
        }
        
        public override void SetView(MonoView monoView)
        {
            view = monoView as PlayLevelDialog;

            SetupTitle();
            SetupStars();
        }
        
        public override void Teardown()
        {
        }

        private void SetupTitle()
        {
            view.SetLevelTitle("Level 123");
        }
        
        private void SetupStars()
        {
            int starsCount = 2;
            for (int i = 0; i < starsCount; ++i)
            {
                view.DialogStarsView.ShowStar(i);
            }
        }
    }
}

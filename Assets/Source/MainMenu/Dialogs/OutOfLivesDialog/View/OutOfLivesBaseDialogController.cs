﻿using Claw.UI;
using Zenject;
using Claw;
using Claw.Ads;
using GlassCrash.MainMenu.Lives;
using GlassCrash.App.User;
using GlassCrash.App.Features;
using GlassCrash.DebugStuff;

namespace GlassCrash.MainMenu.Dialogs
{
    public class OutOfLivesBaseDialogController : DoubleChoiceDialogViewController
    {
        OutOfLivesDialog view;

        [Inject]
        protected FeatureFlags ff;
        
        [Inject]
        protected DebugModel debugModel;
        
        [Inject]
        AdInterstitialService adInterstitialService;
        
        [Inject]
        AdRewardedService adRewardedService;

        [Inject]
        protected IFakeAdRewardedService fakeAdRewardedService;
        
        [Inject]
        CommandsQueue commandsQueue;

        [Inject]
        protected UserLivesModel userLivesModel;

        public override void SetView(MonoView monoView)
        {
            base.SetView(monoView);
            
            SetupControllerBehaviour(monoView);
            
            RegisterRewardedAdHandlers();
            RegisterInterstitialAdHandlers();
        }
        
        public override void Teardown()
        {
            base.Teardown();
            
            UnregisterRewardedAdHandlers();
            UnregisterInterstitialAdHandlers();
            
            TeardownControllerBehoaviour();
        }

        #region Setup & Teardown to override

        protected virtual void SetupControllerBehaviour(MonoView monoView)
        {
            view = monoView as OutOfLivesDialog;
            
            view.watchAdSignal += OnWatchAd;
            view.watchAdDebugSignal += OnWatchAdDebug;

            userLivesModel.livesCountChangedSignal += OnLivesCountChanged;

            UpdateWatchButtonVisibility();
            
            // Try if dialog shows back after moves were added.
            OnLivesCountChanged();
            
            ConfigureDebug();
        }
        
        protected virtual void TeardownControllerBehoaviour()
        {
            userLivesModel.livesCountChangedSignal -= OnLivesCountChanged;

            view.watchAdSignal -= OnWatchAd;
            view.watchAdDebugSignal -= OnWatchAdDebug;
        }
        
        #endregion 
        

        protected virtual void UpdateWatchButtonVisibility()
        {
            view.WatchAdButton.interactable = IsRewardedVideoAdLoaded || IsRewardedInterstitialAdLoaded;
        }

        private void OnLivesCountChanged()
        {
            if (userLivesModel.LivesCount > 0)
            {
                view.IsChoiceApproved = true;
                view.Hide();
            }
        }

        protected virtual void OnWatchAd()
        {
            if (IsRewardedVideoAdLoaded)
            {
                commandsQueue.Enqueue<WatchOutOfLivesRewardAdCommand>();
            }
            else if (IsRewardedInterstitialAdLoaded)
            {
                commandsQueue.Enqueue<WatchOutOfLivesInterstitialRewardAdCommand>();
            }
        }

        protected virtual void OnWatchAdDebug()
        {
            fakeAdRewardedService.FakeAdDidRewardAndClose(1, "LivesCount");
        }
        
        private void ConfigureDebug()
        {
            view.EnableDebugElements(debugModel.IsDebug);
        }

        #region RewardedAd

        protected bool IsRewardedVideoAdLoaded {
            get {
                if (ff.IsAdsEnabled)
                {
                    return adRewardedService.IsRewardedAdLoaded(); 
                }
                else return false;
            }
        }

        private void RegisterRewardedAdHandlers()
        {
            adRewardedService.onAdClosed += UpdateWatchButtonVisibility;
            adRewardedService.onAdLoaded += UpdateWatchButtonVisibility;
            adRewardedService.onAdFailedToLoad += UpdateWatchButtonVisibility;
        }

        private void UnregisterRewardedAdHandlers()
        {
            adRewardedService.onAdFailedToLoad -= UpdateWatchButtonVisibility;
            adRewardedService.onAdLoaded -= UpdateWatchButtonVisibility;
            adRewardedService.onAdClosed -= UpdateWatchButtonVisibility;
        }

        #endregion
        
        #region InterstitialAd
        
        protected bool IsRewardedInterstitialAdLoaded { 
            get {
                if (ff.IsAdsEnabled && ff.IsRewardedInterstitialEnabled)
                {
                    return adInterstitialService.IsLoaded(AdInterstitialService.Interstitial_MainMenu);
                }
                else return false;
            }
        }
        
        private void RegisterInterstitialAdHandlers()
        {
            adInterstitialService.onAdClosed += UpdateWatchButtonVisibility;
            adInterstitialService.onAdLoaded += UpdateWatchButtonVisibility;
            adInterstitialService.onAdFailedToLoad += UpdateWatchButtonVisibility;
        }

        private void UnregisterInterstitialAdHandlers()
        {
            adInterstitialService.onAdFailedToLoad -= UpdateWatchButtonVisibility;
            adInterstitialService.onAdLoaded -= UpdateWatchButtonVisibility;
            adInterstitialService.onAdClosed -= UpdateWatchButtonVisibility;
        }
        
        #endregion
    }
}

﻿using System;
using Claw.UI;
using GlassCrash.MainMenu.Tracking;
using Zenject;

namespace GlassCrash.MainMenu.Dialogs
{
    public class OutOfLivesAppDialogController : OutOfLivesBaseDialogController
    {
        [Inject]
        MainMenuTrackingService track;
        
        protected override void OnDialogCancel(DoubleChoiceDialogView dialogView)
        {
            base.OnDialogCancel(dialogView);
            
            if (ff.IsTrackingEnabled)
            {
                track.SagaOutOfLivesCancelClick();
            }
        }

        protected override void OnWatchAd()
        {
            base.OnWatchAd();
            
            if (ff.IsTrackingEnabled)
            {
                track.SagaOutOfLivesWatchAdClick();
            }
        }
    }
}

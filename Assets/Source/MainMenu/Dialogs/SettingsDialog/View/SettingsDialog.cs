﻿using System;
using Claw.UI;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace GlassCrash.MainMenu.Dialogs
{
    public class SettingsDialog : DoubleChoiceDialogView
    {
        public event Action<bool> fxToggledSignal;
        public event Action<bool> musicToggledSignal;
        
        public event Action debugToggledSignal;
        
        private Text levelTitle;

        private Toggle FxToggle { get { return Resolver.FindGameObjectAnywhere("FxToggle").GetComponent<Toggle>(); } }
        private Toggle MusicToggle { get { return Resolver.FindGameObjectAnywhere("MusicToggle").GetComponent<Toggle>(); } }
        
        private Button DebugButton { get { return Resolver.FindGameObjectAnywhere("DebugButton").GetComponent<Button>(); } }
        private Image DebugButtonImage { get { return Resolver.FindGameObjectAnywhere("DebugButton/Image").GetComponent<Image>(); } }
        
        public override void Setup()
        {
            base.Setup();
            
            levelTitle = Resolver.FindGameObjectAnywhere("Panel/LevelTitle").GetComponent<Text>();
            
            FxToggle.onValueChanged.AddListener(OnFxToggleValueChanged);
            MusicToggle.onValueChanged.AddListener(OnMusicToggleValueChanged);
            RegisterDebugButtonListener();
        }

        public override void Deactivate()
        {
            FxToggle.onValueChanged.RemoveAllListeners();
            MusicToggle.onValueChanged.RemoveAllListeners();
            DebugButton.onClick.RemoveAllListeners();
        }

        public void SetFxToggled(bool value)
        {
            FxToggle.isOn = value;
        }
        
        public void SetMusicToggled(bool value)
        {
            MusicToggle.isOn = value;
        }
        
        private void OnFxToggleValueChanged(bool value)
        {
            if (fxToggledSignal != null) fxToggledSignal(value);
        }
        
        private void OnMusicToggleValueChanged(bool value)
        {
            if (musicToggledSignal != null) musicToggledSignal(value);
        }
        
        public void SetDebugButtonVisble(bool visible)
        {
            var c = DebugButtonImage.color;
            c.a = visible ? 0.5f : 0.0f;
            DebugButtonImage.color = c;
        }
        
        private void RegisterDebugButtonListener()
        {
            var clickStream = DebugButton.onClick.AsObservable();
            
            clickStream.Buffer(clickStream.Throttle(TimeSpan.FromMilliseconds(200 * Time.timeScale)))
                .Where(xs => xs.Count >= 10)
                    .Subscribe(xs =>
            {
                Debug.Log("Debug toggle detected! Count:" + xs.Count);
                if (debugToggledSignal != null) { debugToggledSignal(); }
            });
            
        }
    }
}

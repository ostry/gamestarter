﻿using System;
using Claw.UI;
using UnityEngine;
using Claw;
using Zenject;
using GlassCrash.App.Audio;
using GlassCrash.DebugStuff;

namespace GlassCrash.MainMenu.Dialogs
{
    public class SettingsDialogController : ViewController
    {
        SettingsDialog view;

        [Inject]
        AppAudioModel appAudioModel;
        
        [Inject]
        CommandsQueue commandsQueue;

        [Inject]
        DebugModel debugModel;
        
        public override void SetView(MonoView monoView)
        {
            view = monoView as SettingsDialog;

            view.fxToggledSignal += OnFxToggled;
            view.musicToggledSignal += OnMusicToggled;
            view.debugToggledSignal += OnDebugToggled;

            debugModel.isDebugChangedSignal += OnIsDebugChanged;

            OnIsDebugChanged();
            SetupSettings();
        }

        public override void Teardown()
        {
            debugModel.isDebugChangedSignal -= OnIsDebugChanged;
            
            view.debugToggledSignal -= OnDebugToggled;
            view.musicToggledSignal -= OnMusicToggled;
            view.fxToggledSignal -= OnFxToggled;
        }
        
        private void SetupSettings()
        {
            bool isFxOn = appAudioModel.IsFXEnabled;
            bool isMusicOn = appAudioModel.IsMusicEnabled;
            
            view.SetFxToggled(isFxOn);
            view.SetMusicToggled(isMusicOn);
        }
        
        private void OnFxToggled(bool value)
        {
            if (value)
            {
                commandsQueue.Enqueue<EnableFxSoundsCommand>();
            }
            else
            {
                commandsQueue.Enqueue<DisableFxSoundsCommand>();
            }
        }
        
        private void OnMusicToggled(bool value)
        {
            if (value)
            {
                commandsQueue.Enqueue<EnableMusicCommand>();
            }
            else
            {
                commandsQueue.Enqueue<DisableMusicCommand>();
            }
        }
        
        private void OnDebugToggled()
        {
            commandsQueue.Enqueue<ToggleDebugModeCommand>();
        }
        
        private void OnIsDebugChanged()
        {
            var visible = debugModel.IsDebug;
            view.SetDebugButtonVisble(visible);
        }
    }
}

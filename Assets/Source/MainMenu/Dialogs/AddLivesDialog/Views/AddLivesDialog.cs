﻿using System;
using UnityEngine.UI;
using Claw.UI;

namespace GlassCrash.MainMenu.Dialogs
{
    public class AddLivesDialog : DoubleChoiceDialogView
    {
        public event Action watchAdSignal;
        public event Action watchAdDebugSignal;

        public Button WatchAdButton { get { return Resolver.FindGameObjectAnywhere("WatchAd").GetComponent<Button>(); } }
        private Button WatchAdDebugButton { get { return Resolver.FindGameObjectAnywhere("WatchAdDebug").GetComponent<Button>(); } }
        
        public void OnWatchAdd()
        {
            if (watchAdSignal != null)
                watchAdSignal();
        }
        
        public void OnWatchAdDebug()
        {
            if (watchAdDebugSignal != null)
                watchAdDebugSignal();
        }
        
        public void EnableDebugElements(bool enable)
        {
            WatchAdDebugButton.gameObject.SetActive(enable);
        }
    }
}

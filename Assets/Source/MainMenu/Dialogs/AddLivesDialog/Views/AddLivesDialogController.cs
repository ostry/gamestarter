﻿using System;
using Claw.UI;
using Zenject;
using Claw;
using GlassCrash.Game;
using Claw.UI.Dialogs;
using Claw.Animation.Tweening;
using Claw.Utils;
using Claw.Ads;
using GlassCrash.MainMenu.Lives;
using GlassCrash.App.User;
using GlassCrash.MainMenu.Ads;
using GlassCrash.MainMenu;
using GlassCrash.DebugStuff;
using UnityEngine;

namespace GlassCrash.MainMenu.Dialogs
{
    public class AddLivesDialogController : OutOfLivesBaseDialogController
    {
        AddLivesDialog view;

        private int initialLivesCount;

        protected override void SetupControllerBehaviour(MonoView monoView)
        {
            view = monoView as AddLivesDialog;

            view.watchAdSignal += OnWatchAd;
            view.watchAdDebugSignal += OnWatchAdDebug;

            initialLivesCount = userLivesModel.LivesCount;
            userLivesModel.livesCountChangedSignal += OnLivesCountChanged;
            
            UpdateWatchButtonVisibility();

            ConfigureDebug();
        }
        
        protected override void TeardownControllerBehoaviour()
        {
            userLivesModel.livesCountChangedSignal -= OnLivesCountChanged;

            view.watchAdSignal -= OnWatchAd;
            view.watchAdDebugSignal -= OnWatchAdDebug;
        }
        
        private void OnLivesCountChanged()
        {
            if (userLivesModel.LivesCount > initialLivesCount)
            {
                view.IsChoiceApproved = true;
                view.Hide();
            }
        }
        
        protected override void UpdateWatchButtonVisibility()
        {
            view.WatchAdButton.interactable = IsRewardedVideoAdLoaded || IsRewardedInterstitialAdLoaded;
        }
        
        private void ConfigureDebug()
        {
            view.EnableDebugElements(debugModel.IsDebug);
        }
    }
}

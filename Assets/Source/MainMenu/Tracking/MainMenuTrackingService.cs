﻿using System.Collections.Generic;
using Claw.Tracking;
using Zenject;
using GlassCrash.App.User;

namespace GlassCrash.MainMenu.Tracking
{
    public class MainMenuTrackingService
    {
        [Inject]
        ITrackingService ts;

        [Inject]
        UserLivesModel userLivesModel;

        [Inject]
        UserRequests userRequests;

        private Dictionary<string, object> DefaultParamsPlus(Dictionary<string, object> parameters)
        {
            return parameters;
        }

        #region MainMenu

        public void MainMenuInit()
        {
            ts.LogEvent("MainMenuInit", DefaultParamsPlus(new Dictionary<string, object>() { }));
        }

        public void MainMenuExit()
        {
            ts.LogEvent("MainMenuExit", DefaultParamsPlus(new Dictionary<string, object>() { }));
        }

        #endregion

        #region Levels

        public void PlayLevelDialog()
        {
            int selectedLevelId = 1;

            ts.LogEvent("PlayLevelDialog", DefaultParamsPlus(new Dictionary<string, object>() {
                {"level", selectedLevelId}
            }));
        }

        #endregion

        #region SagaLivesView
        
        public void AddLive()
        {
            int livesCount = userLivesModel.LivesCount;
            double remainingTimeToNextLifeInSeconds = userRequests.RequestNextLifeRestorationRemainingTimeInSeconds();
            
            ts.LogEvent("AddLive", DefaultParamsPlus(new Dictionary<string, object>() {
                {"livesCount", livesCount},
                {"newLifeInSeconds", remainingTimeToNextLifeInSeconds}
            }));
        }
        
        #endregion

        #region OutOfLives

        public void SagaOutOfLives()
        {
            ts.LogEvent("SagaOutOfLives", DefaultParamsPlus(new Dictionary<string, object>() { }));
        }

        public void SagaOutOfLivesWatchAdClick()
        {
            ts.LogEvent("SagaOutOfLivesWatchAdClick", DefaultParamsPlus(new Dictionary<string, object>() { }));
        }
        public void SagaOutOfLivesCancelClick()
        {
            ts.LogEvent("SagaOutOfLivesCancelClick", DefaultParamsPlus(new Dictionary<string, object>() { }));
        }

        public void SagaOutOfLivesAdRewardAwarded()
        {
            ts.LogEvent("SagaOutOfLivesAdRewardAwarded", DefaultParamsPlus(new Dictionary<string, object>() { }));
        }

        #endregion

    }
}

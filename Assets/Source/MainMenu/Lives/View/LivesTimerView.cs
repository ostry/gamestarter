﻿using System;
using System.Collections.Generic;
using Claw.UI;
using UnityEngine.UI;
using Claw.UI.Components;

namespace GlassCrash.MainMenu.Lives
{
    public class LivesTimerView : MonoView
    {
        public event Action addLifeSignal;
        private void NotifyAddLife() { if (addLifeSignal != null) addLifeSignal(); }
        
        private Text RemainingTimeText { get { return Resolver.FindGameObjectAnywhere("TimerText").GetComponent<Text>(); } }
        
        public override void Setup()
        {
        }
        
        public void SetRemainingTimeInSeconds(double timeInSeconds)
        {
            RemainingTimeText.text = TextFormatters.SecondsToTimeFormatter(timeInSeconds);
        }
        
        public void OnAddLife()
        {
            NotifyAddLife();
        }
    }
}

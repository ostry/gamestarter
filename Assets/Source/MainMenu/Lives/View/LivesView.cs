﻿using System;
using Claw.UI;

namespace GlassCrash.MainMenu.Lives
{
    public class LivesView : MonoView
    {
        public HeartsView HeartsView { get { return ViewsResolver.Resolve<HeartsView>(); } }
        public LivesTimerView LivesTimerView { get { return ViewsResolver.Resolve<LivesTimerView>(); } }
        
        public override void Setup()
        {
            HeartsView.SetActive(true);
            LivesTimerView.SetActive(true);
        }
    }
}

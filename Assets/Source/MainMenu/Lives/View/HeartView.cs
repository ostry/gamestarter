﻿using System;
using Claw.UI;
using UnityEngine;

namespace GlassCrash.MainMenu.Lives
{
    public class HeartView : MonoView
    {
        private GameObject HeartFull { get { return Resolver.FindGameObjectAnywhere("HeartFull"); } }
        private GameObject HeartEmpty { get { return Resolver.FindGameObjectAnywhere("HeartEmpty"); } }
        
        public override void Setup()
        {
            ShowHeartFull();
        }
        
        public void ShowHeartFull()
        {
            HeartFull.SetActive(true);
            HeartEmpty.SetActive(false);
        }
     
        public void ShowHeartEmpty()
        {
            HeartFull.SetActive(false);
            HeartEmpty.SetActive(true);
        }
    }
}

﻿using System;
using Claw.UI;
using GlassCrash.App.User;
using Zenject;

namespace GlassCrash.MainMenu.Lives
{
    public class LivesViewController : ViewController
    {
        [Inject]
        UserLivesModel userLivesModel;

        LivesView view;

        public override void SetView(MonoView monoView)
        {
            view = monoView as LivesView;

            userLivesModel.livesCountChangedSignal += OnLivesCountChanged;
            
            view.HeartsView.SetMaxLivesCount(userLivesModel.MaxLivesCount);
            
            OnLivesCountChanged();
        }

        public override void Teardown()
        {
            userLivesModel.livesCountChangedSignal -= OnLivesCountChanged;
        }

        private void OnLivesCountChanged()
        {
            int livesCount = userLivesModel.LivesCount;
            view.HeartsView.SetFullHearts(livesCount);

            UpdateLivesTimerVisibility();
        }

        private void UpdateLivesTimerVisibility()
        {
            bool visible = !userLivesModel.IsLivesFull;
            view.LivesTimerView.SetActive(visible);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Claw.UI;

namespace GlassCrash.MainMenu.Lives
{
    public class HeartsView : MonoView
    {
        private List<HeartView> hearts;
        private int maxLivesCount;
        
        public List<HeartView> Hearts
        {
            get
            {
                var thisRoot = Resolver.FindGameObjectAnywhere("HeartsView");
                
                if (hearts == null)
                {
                    hearts = new List<HeartView>();
                    hearts.AddRange(ViewsResolver.ResolveRecursivelyInside<HeartView>("HeartsView"));
                }
                return hearts;
            }
        }
        
        public override void Setup()
        {
            Hearts.ForEach((heartView) => { heartView.SetActive(false); });
        }
        
        public void SetMaxLivesCount(int maxLivesCount)
        {
            this.maxLivesCount = maxLivesCount;
            for (int i = 0; i < maxLivesCount; ++i)
            {
                hearts[i].SetActive(true);
            }
        }
        
        public void SetFullHearts(int fullHeartsCount)
        {
            for (int i = 0; i < maxLivesCount; ++i)
            {
                hearts[i].SetActive(true);
                
                if (i < fullHeartsCount)
                    hearts[i].ShowHeartFull();
                else
                    hearts[i].ShowHeartEmpty();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Claw.UI;
using UnityEngine.UI;
using Claw.UI.Components;
using System.Collections;
using UnityEngine;
using Zenject;
using GlassCrash.App.User;
using GlassCrash.MainMenu.Events;
using Claw.UI.Dialogs;
using Claw;

namespace GlassCrash.MainMenu.Lives
{
    public class LivesTimerViewController : ViewController
    {
        [Inject]
        UserRequests userRequests;

        [Inject]
        MainMenuRequests mainMenuRequests;
        
        LivesTimerView view;
        
        public override void SetView(MonoView monoView)
        {
            view = monoView as LivesTimerView;

            view.addLifeSignal += OnAddLife;
            
            StartCoroutine("UpdateTimer");
        }

        public override void Teardown()
        {
            view.addLifeSignal -= OnAddLife;
            
            StopAllCoroutines();
        }
        
        private void OnAddLife()
        {
            mainMenuRequests.RequestSetState<AddLivesState>();
        }
        
        IEnumerator UpdateTimer()
        {
            while(true)
            {
                var lifeRemainingTime = userRequests.RequestNextLifeRestorationRemainingTimeInSeconds();
                view.SetRemainingTimeInSeconds(lifeRemainingTime);

                yield return new WaitForSeconds(0.2f);
            }
        }
    }
}

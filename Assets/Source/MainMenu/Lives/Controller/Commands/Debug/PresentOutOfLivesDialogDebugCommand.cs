﻿using System;
using Claw;
using GlassCrash.App.User;
using Zenject;
using GlassCrash.MainMenu;

namespace GlassCrash.MainMenu.Lives
{
    public class PresentOutOfLivesDialogDebugCommand : Command
    {
        [Inject]
        CommandsQueue commandsQueue;

        public override void Execute()
        {
            commandsQueue.Enqueue<GoMainMenu<OutOfLivesDialogState>>();
        }
    }
}

﻿using System;
using Claw;
using GlassCrash.App.User;
using Zenject;

namespace GlassCrash.MainMenu.Lives
{
    public class RestoreLifeDebugCommand : Command
    {
        [Inject]
        UserLivesModel userLivesModel;

        public override void Execute()
        {
            userLivesModel.RestoreSingleLife();
        }
    }
}

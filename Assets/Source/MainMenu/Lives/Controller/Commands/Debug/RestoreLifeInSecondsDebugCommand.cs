﻿using System;
using Claw;
using GlassCrash.App.User;
using Zenject;

namespace GlassCrash.MainMenu.Lives
{
    public class RestoreLifeInSecondsDebugCommand : Command
    {
        [Inject]
        UserLivesService userLivesService;

        public override void Execute()
        {
            userLivesService.RestoreLifeInSeconds(5);
        }
    }
}

﻿using System;
using Claw;
using Zenject;
using GlassCrash.App.User;

namespace GlassCrash.MainMenu.Lives
{
    public class LooseAllLifesDebugComnand : Command
    {
        [Inject]
        UserLivesModel userLivesModel;
        
        public override void Execute()
        {
            while(userLivesModel.LivesCount > 0)
            {
                userLivesModel.LooseLife();
            }
        }
    }
}

﻿using System;
using Claw;
using Zenject;
using GlassCrash.App.User;

namespace GlassCrash.MainMenu.Lives
{
    public class LooseLifeDebugComnand : Command
    {
        [Inject]
        UserLivesModel userLivesModel;
        
        public override void Execute()
        {
            userLivesModel.LooseLife();
        }
    }
}

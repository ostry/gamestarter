﻿using System;
using Claw;
using Claw.Ads;
using GlassCrash.App.Features;
using Zenject;

namespace GlassCrash.MainMenu.Lives
{
    public class WatchOutOfLivesRewardAdCommand : Command
    {
        [Inject]
        FeatureFlags ff;
        
        [Inject]
        AdRewardedService adRewardedService;

        public override void Execute()
        {
            if (ff.IsAdsEnabled)
            {
                adRewardedService.PresentRewardAd();    
            }
        }
    }
}

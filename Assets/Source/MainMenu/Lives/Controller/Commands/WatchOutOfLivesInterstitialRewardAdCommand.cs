﻿using System;
using Claw;
using Claw.Ads;
using GlassCrash.App.Features;
using Zenject;

namespace GlassCrash.MainMenu.Lives
{
    public class WatchOutOfLivesInterstitialRewardAdCommand : Command
    {
        [Inject]
        FeatureFlags ff;
        
        [Inject]
        AdInterstitialService adInterstitialService;

        public override void Execute()
        {
            if (ff.IsAdsEnabled)
            {
                adInterstitialService.PresentIntersitial(AdInterstitialService.Interstitial_MainMenu, true);
            }
        }
    }
}

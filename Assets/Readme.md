﻿**Git submodules**

Followed this example http://prime31.github.io/A-Method-for-Working-with-Shared-Code-with-Unity-and-Git/

*Claw example:*

- Place submodule clone into GlassCrash/submodules/Claw

```
cd GlassCrash/submodules
git submodule add git@bitbucket.org:moty-sharpies/claw.git Claw

```
(You may need the git submodule add --force ... if you are trying adding the same submodule more that once ;)

- Create library symlinked folder for the submodule sources

```
cd GlassCrah/Assets/Libraries
ln -s ../../submodules/Claw Claw
```

(Note: Putting sources unded Plugins folder would make Unity build them into dll, but that is just for fully selfcontained libraries - project is not a dependency for such "plugin")

- Unity will then keep .meta files into separate folder under GlassCrash/Assets/submodules/ folder. Therw will be empty tree structure Claw with Claw.meta and recursively so on ...

And that should be it ! ;) Hope it works in the future. Cheers from 17/07/2018 ! :D


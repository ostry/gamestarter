Old versions of Firebase Unity sdk:
https://dl.google.com/firebase/sdk/unity/firebase_unity_sdk_${version}.zip
was https://dl.google.com/firebase/sdk/unity/firebase_unity_sdk_4.5.2.zip
now is https://dl.google.com/firebase/sdk/unity/firebase_unity_sdk_4.5.2.zip

also GoogleMobileAds has a wrong dep specified, needed to bump it from 12.0.1 to 15.0.1 in GoogleMobileAds/Editor/GoogleMobileAdsDependencies.xml
<androidPackage spec="com.google.android.gms:play-services-ads:15.0.1">

﻿using System;
using GlassCrash.LevelPlayer;

namespace LevelPlayerApp
{
    class Program
    {
        static void Main (string [] args)
        {
            LevelPlayer levelPlayer = new LevelPlayer ();
            levelPlayer.Init ();
            
            levelPlayer.Run ();
            
            Environment.Exit (0);
        }
    }
}
